@extends('layouts.app')

@section('title-head')
Cadastro de Usuários
@endsection

@section('title-body')
Cadastro de Usuários
@endsection

@section('page-css')
<!-- daterange picker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

<!-- bootstrap datepicker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/iCheck/all.css') }}">

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/select2/dist/css/select2.min.css') }}">

@endsection
@section('main-content')
<section class="content">

    <div class="row">
        <div class="col-lg-12 col-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de usuários</h3>
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                    </div>
                    @endforeach
                    @endif
                    
                    <form action="{{ route('cadastros.usuario.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Nome de usuário</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" value="{{ old('username') }}" placeholder="Nome de usuário" name="username" required>
                            </div>
                            <!-- /.input group -->
                        </div>                    

                        <div class="form-group">
                            <label>Tipo de usuário</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-group"></i>
                                    </div>
                                </div>
                                <select class="form-control" name="tipo" required>                                    
                                    @if(Auth::user()->tipo == 'Master' or Auth::user()->tipo == 'Administrador')
                                    
                                    @if(Auth::user()->tipo == 'Master')
                                    <option value="Administrador">Master</option>
                                    @endif
                                    
                                    <option value="Administrador">Administrador</option>
                                    <option value="Colaborador">Colaborador</option>
                                    <option value="Entregador">Entregador</option>
                                    @endif                                
                                    <option value="Cliente">Cliente</option>                                
                                </select>
                            </div>
                            <!-- /.input group -->
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg pull-right">Cadastrar</button>                            
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>            
        </div>        
    </div>
      
    </section>

@endsection
@section('page-js')

<!-- InputMask -->
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
	
	<!-- date-range-picker -->
	<script src="{{ asset('public/assets/vendor_components/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	
	<!-- bootstrap datepicker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	
	<!-- bootstrap color picker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
	
	<!-- bootstrap time picker -->
	<script src="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
	
	<!-- SlimScroll -->
	<script src="{{ asset('public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- iCheck 1.0.1 -->
	<script src="{{ asset('public/assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
	
	<!-- FastClick -->
	<script src="{{ asset('public/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>     
@endsection
