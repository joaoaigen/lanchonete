<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('../img/favicon1.png') }}">

    <title>Garagem Lanches - Criar pedido </title>
  
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    
    <header class="main-header">
        <!-- Logo -->
        <a href="index.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <b class="logo-mini">
                <!--<span class="light-logo"><img src="{{ asset('public/images/imagem.png') }}" alt="logo"></span>
                <span class="dark-logo"><img src="{{ asset('public/images/imagem.png') }}" alt="logo"><h1>Garagem Lanches</h1></span>-->
            </b>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
		  <!--<img src="{{ asset('public/images/imagem.png') }}" alt="logo" class="light-logo">
	  	  <img src="{{ asset('public/images/imagem.png') }}" alt="logo" class="dark-logo">-->
	  </span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top">   
            <div class="navbar-custom-menu">               
            </div>
        </nav>
    </header>
    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cadastrar um pedido
      </h1>      
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Preencha as informações abaixo corretamente para cadastrar</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        @if (isset($errors) && count($errors) > 0)
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                        </div>
                        @endforeach
                        @endif

                        <form action="{{ route('cadastros.pedidos.store') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 col-xl-6 col-12">                                
                                    <div class="form-group">
                                        <label>Mesa</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-edit"></i>
                                                </div>
                                            </div>
                                            <select class="form-control" name="mesa" required>
                                                <option value="0" selected>Selecione uma mesa</option>
                                                @foreach($mesas as $row)
                                                @if($row->status == 'Disponivel')                                                
                                                <option value="{{ $row->id }}">{{ 'Mesa: ' . $row->numero }}</option>
                                                @endif
                                                @endforeach                                            
                                            </select>
                                        </div>                            
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-6 col-12">
                                    <div class="form-group">
                                        <label>Usuário</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            <select class="form-control" name="usuario" required> 
                                                @foreach($usuario as $row)
                                                <option value="{{ $row->id }}">{{ $row->username }}</option>
                                                @endforeach
                                            </select>                                     
                                        </div>                            
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Produtos</label>
                                <select class="form-control select2 w-p100" style="color: #252525" multiple="multiple" id="produtos" data-placeholder="Selecione o produto" name="produtos[]" required> 
                                    @foreach($produtos as $row)
                                    <option value="{{ $row->id }}" id="{{ ' '. $row->id .' ' }}">{{ $row->nome }}</option>
                                    @endforeach
                                </select>  

                                <input hidden id="prod" value="{{$id_produtos}}">
                            </div>

                            <div class="row" id="addProdutos">

                            </div>

                            <div class="row">
                                <div class="col-md-6 col-xl-6 col-12">                                
                                    <div class="form-group">
                                        <label>Tipo</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-edit"></i>
                                                </div>
                                            </div>
                                            <select class="form-control" name="tipo" required>                                             
                                                <option value="Local">Local</option>                                         
                                                <option value="Viagem">Viagem</option>
                                            </select>
                                        </div>                            
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-6 col-12">
                                    <div class="form-group">
                                        <label>Total</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                            </div>
                                            <input class="form-control money2" id="total" name="total" required>                                   
                                        </div>                            
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg pull-right">Cadastrar</button>                            
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>            
            </div>    
            <div class="col-md-12 col-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pedidos abertos</h3>              
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="mesas" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                    <tr class="bg-dark">
                                        <th style="text-align: center">EMPRESA</th>
                                        <th style="text-align: center">USUÁRIO</th>
                                        <th style="text-align: center">PRODUTOS</th>
                                        <th style="text-align: center">MESA</th>
                                        <th style="text-align: center">TIPO</th>
                                        <th style="text-align: center">SITUAÇÃO</th>
                                        <th style="text-align: center">STATUS</th>
                                        <th style="text-align: center">TOTAL</th>
                                        <th style="text-align: center">ATUALIZADO EM</th>
                                        @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                        <th style="text-align: center">OPÇÕES</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>	
                                    @foreach($pedidos as $row)
                                    @if($row->status == 'Aberto')
                                    <tr>                                
                                        <td style="text-align: center">{{ DB::table('empresas')->where('id', '=', $row->empresa)->first()->nome_exib }}</td>
                                        <td style="text-align: center">{{ DB::table('users')->where('id', '=', $row->usuario)->first()->username }}</td>
                                        <td style="text-align: center">{{ $row->produtos }}</td>
                                        <td style="text-align: center">{{ isset(DB::table('mesas')->where('id', '=', $row->mesa)->first()->numero) ? DB::table('mesas')->where('id', '=', $row->mesa)->first()->numero : '' }}</td>

                                        @if($row->tipo == 'Viagem')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs"><i class="fa fa-motorcycle" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                        @else
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                        @endif

                                        @if($row->tipo == 'Viagem')
                                        @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Entrega')
                                        <td style="text-align: center"><span class="btn btn-default btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @endif

                                        @if($row->tipo == 'Local')
                                        @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_local }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_local }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_local }}</span></td>  
                                        @endif

                                        @endif
                                        <td style="text-align: center"><span class="btn btn-{{ $row->status == 'Aberto' ? 'primary' : $row->status == 'Concluido' ? 'success' : 'danger' }} btn-xs">{{ $row->status }}</span></td>
                                        <td style="text-align: center">{{ $row->total }}</td>                                
                                        <td style="text-align: center">{{ date( 'd/m/Y H:i:s', strtotime($row->updated_at)) }}</td>     

                                        @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master' && $row->status != 'Cancelado')
                                        <td style="text-align: center">
                                            <a class="btn btn-success btn-xs" id="fechar_pedido" href="{{ url('pedidos/fechar-pedido/'.$row->id) }}">FINALIZAR</a> 
                                            <br>
                                            <a class="btn btn-danger btn-xs" id="cancelar_pedido" href="#" onclick="alerta({{ $row->id }});">CANCELAR</a>
                                        </td>                                
                                        @elseif(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                        <td style="text-align: center"></td>
                                        @endif
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>				  
                                <tfoot>
                                    <tr class="bg-dark">
                                        <th style="text-align: center">EMPRESA</th>
                                        <th style="text-align: center">USUÁRIO</th>
                                        <th style="text-align: center">PRODUTOS</th>
                                        <th style="text-align: center">MESA</th>
                                        <th style="text-align: center">TIPO</th>
                                        <th style="text-align: center">SITUAÇÃO</th>
                                        <th style="text-align: center">STATUS</th>
                                        <th style="text-align: center">TOTAL</th>
                                        <th style="text-align: center">ATUALIZADO EM</th>
                                        @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                        <th style="text-align: center">OPÇÕES</th>
                                        @endif
                                    </tr>
                                </tfoot>
                            </table>
                        </div>              
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lista de pedidos</h3>              
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="mesas" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                    <tr class="bg-dark">
                                        <th style="text-align: center">EMPRESA</th>
                                        <th style="text-align: center">USUÁRIO</th>
                                        <th style="text-align: center">PRODUTOS</th>
                                        <th style="text-align: center">MESA</th>
                                        <th style="text-align: center">TIPO</th>
                                        <th style="text-align: center">SITUAÇÃO</th>
                                        <th style="text-align: center">STATUS</th>
                                        <th style="text-align: center">TOTAL</th>
                                        <th style="text-align: center">ATUALIZADO EM</th>
                                        @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                        <th style="text-align: center">OPÇÕES</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>	
                                    @foreach($pedidos as $row)
                                    @if($row->status != 'Aberto')
                                    <tr>                                
                                        <td style="text-align: center">{{ DB::table('empresas')->where('id', '=', $row->empresa)->first()->nome_exib }}</td>
                                        <td style="text-align: center">{{ DB::table('users')->where('id', '=', $row->usuario)->first()->username }}</td>
                                        <td style="text-align: center">{{ $row->produtos }}</td>
                                        <td style="text-align: center">{{ isset(DB::table('mesas')->where('id', '=', $row->mesa)->first()->numero) ? DB::table('mesas')->where('id', '=', $row->mesa)->first()->numero : '' }}</td>

                                        @if($row->tipo == 'Viagem')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs"><i class="fa fa-motorcycle" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                        @else
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                        @endif

                                        @if($row->tipo == 'Viagem')
                                        @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Entrega')
                                        <td style="text-align: center"><span class="btn btn-default btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_viagem }}</span></td>  
                                        @endif
                                        @endif

                                        @if($row->tipo == 'Local')
                                        @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_local }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_local }}</span></td>  
                                        @endif
                                        @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_local }}</span></td>  
                                        @endif

                                        @endif
                                        <td style="text-align: center"><span class="btn btn-{{ $row->status == 'Aberto' ? 'primary' : $row->status == 'Concluido' ? 'success' : 'danger' }} btn-xs">{{ $row->status }}</span></td>
                                        <td style="text-align: center">{{ $row->total }}</td>                                
                                        <td style="text-align: center">{{ date( 'd/m/Y H:i:s', strtotime($row->updated_at)) }}</td>     

                                        @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master' && $row->status != 'Cancelado')                                
                                        <td style="text-align: center"><a class="btn btn-danger btn-xs" id="cancelar_pedido" href="#" onclick="alerta({{ $row->id }});">CANCELAR</a></td>
                                        @elseif(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                        <td style="text-align: center"></td>
                                        @endif
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>				  
                                <tfoot>
                                    <tr class="bg-dark">
                                        <th style="text-align: center">EMPRESA</th>
                                        <th style="text-align: center">USUÁRIO</th>
                                        <th style="text-align: center">PRODUTOS</th>
                                        <th style="text-align: center">MESA</th>
                                        <th style="text-align: center">TIPO</th>
                                        <th style="text-align: center">SITUAÇÃO</th>
                                        <th style="text-align: center">STATUS</th>
                                        <th style="text-align: center">TOTAL</th>
                                        <th style="text-align: center">ATUALIZADO EM</th>
                                        @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                        <th style="text-align: center">OPÇÕES</th>
                                        @endif
                                    </tr>
                                </tfoot>
                            </table>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div> 
   
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">        
    </div>
	  &copy; 2019 Todos os direitos reservados.
  </footer>  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<!-- jQuery 3 -->
	<script src="{{ asset("public/assets/vendor_components/jquery/dist/jquery.min.js") }}"></script>
	
	<!-- popper -->
	<script src="{{ asset('public/assets/vendor_components/popper/dist/popper.min.js') }}"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="{{ asset("public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>
	
	<!-- SlimScroll -->
	<script src="{{ asset("public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js") }}"></script>
	
	<!-- FastClick -->
	<script src="{{ asset("public/assets/vendor_components/fastclick/lib/fastclick.js") }}"></script>
	
        <!-- Crypto_Admin App -->
        <script src="{{ asset("public/js/template.js") }}"></script>

        <!-- Crypto_Admin for demo purposes -->
        <script src="{{ asset("public/js/demo.js") }}"></script>
</body>
</html>
