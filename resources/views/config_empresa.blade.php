<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../images/favicon.ico">

        <title>Wintrom - Definir configurações</title>

        <!-- Bootstrap 4.0-->
        <link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

        <!-- Bootstrap extend-->
        <link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">

        <!-- Crypto_Admin skins -->
        <link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">	

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index.html"><b>Wintrom</b>Gestor</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body pb-20">
                <p class="login-box-msg text-uppercase">Preencha os dados abaixo para finalizar o seu cadastro em nosso sistema.</p>
                @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> Houve alguns problemas com a sua entrada.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ route('admin.cadastrar.empresa') }}" method="post" class="form-element">
                    @csrf
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Nome da empresa" name="nome">                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Nome de exibição no sistema" required name="nome_exib">                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control cnpj" placeholder="CNPJ" name="cnpj" required>                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control telefone" placeholder="Telefone" name="telefone">                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control celular" placeholder="Celular" name="celular" required>                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control cep" onblur="pesquisacep(this.value);" placeholder="CEP" name="cep" id="cep" required>                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Número" name="numero_cep" required>                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Rua" name="rua" id="rua">                        
                    </div>     
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Bairro" readonly name="bairro" id="bairro">                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Cidade" readonly name="cidade" id="cidade">                        
                    </div>      
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="UF" readonly name="uf" id="uf">                        
                    </div>      
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-info btn-block text-uppercase">Cadastrar Empresa</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->


        <!-- jQuery 3 -->
        <script src="{{ asset('public/assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

        <!-- popper -->
        <script src="{{ asset('public/assets/vendor_components/popper/dist/popper.min.js') }}"></script>

        <!-- Bootstrap 4.0-->
        <script src="{{ asset('public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        
        <script src="{{ asset('public/assets/vendor_components/formatter/formatter.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/formatter/jquery.formatter.js') }}"></script>	
	<script src="{{ asset('public/js/jquery.mask.js') }}"></script>	
        <script type="text/javascript" >           
    
            function limpa_formulário_cep() {
                    //Limpa valores do formulário de cep.
                    document.getElementById('rua').value=("");
                    document.getElementById('bairro').value=("");
                    document.getElementById('cidade').value=("");
                    document.getElementById('uf').value=("");
            }

            function meu_callback(conteudo) {
                if (!("erro" in conteudo)) {
                    //Atualiza os campos com os valores.
                    document.getElementById('rua').value=(conteudo.logradouro);
                    document.getElementById('bairro').value=(conteudo.bairro);
                    document.getElementById('cidade').value=(conteudo.localidade);
                    document.getElementById('uf').value=(conteudo.uf);                    
                } //end if.
                else {
                    //CEP não Encontrado.
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                }
            }
        
            function pesquisacep(valor) {

                //Nova variável "cep" somente com dígitos.
                var cep = valor.replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        document.getElementById('rua').value="...";
                        document.getElementById('bairro').value="...";
                        document.getElementById('cidade').value="...";
                        document.getElementById('uf').value="...";                        

                        //Cria um elemento javascript.
                        var script = document.createElement('script');

                        //Sincroniza com o callback.
                        script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                        //Insere script no documento e carrega o conteúdo.
                        document.body.appendChild(script);

                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            };

        </script>

        <script type="text/javascript">
            $(document).ready(function(){
  $('.date').mask('00/00/0000');
  $('.time').mask('00:00:00');
  $('.date_time').mask('00/00/0000 00:00:00');
  $('.cep').mask('00000-000');
  $('.celular').mask('(00) 0 0000-0000');
  $('.telefone').mask('(00) 0000-0000');
  $('.phone_with_ddd').mask('(00) 0000-0000');
  $('.phone_us').mask('(000) 000-0000');
  $('.mixed').mask('AAA 000-S0S');
  $('.cpf').mask('000.000.000-00', {reverse: true});
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  $('.money2').mask("#.##0,00", {reverse: true});
  $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
    translation: {
      'Z': {
        pattern: /[0-9]/, optional: true
      }
    }
  });
  $('.ip_address').mask('099.099.099.099');
  $('.percent').mask('##0,00%', {reverse: true});
  $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
  $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
  $('.fallback').mask("00r00r0000", {
      translation: {
        'r': {
          pattern: /[\/]/,
          fallback: '/'
        },
        placeholder: "__/__/____"
      }
    });
  $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
});
        </script>
    </body>
</html>
