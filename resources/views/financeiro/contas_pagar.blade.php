@extends('layouts.app')

@section('title-head')
Contas a pagar
@endsection

@section('title-body')
Contas a pagar
@endsection

@section('page-css')

<!-- daterange picker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

<!-- bootstrap datepicker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/iCheck/all.css') }}">

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/select2/dist/css/select2.css') }}">

<link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">

<style type="text/css">
    ::-webkit-input-placeholder {
        color: white;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: white;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: white;  
    }

    :-ms-input-placeholder {  
        color: white;  
    }
</style>

@endsection
@section('main-content')
<section class="content">

    <div class="row">
        <div class="col-lg-12 col-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Preencha as informações abaixo corretamente para cadastrar</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                    </div>
                    @endforeach
                    @endif
                    
                    <form action="{{ route('contas.pagar') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-4 col-xl-4 col-12">                                
                                <div class="form-group">
                                    <label>Nome</label>
                                    <input class="form-control" id="nome" placeholder="Digite um nome..." name="nome" required>                                                              
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-4 col-12">
                                <div class="form-group">
                                    <label>Data vencimento</label>
                                    <input class="form-control date" id="data_vencimento" placeholder="Digite a data de vencimento..." name="data_vencimento" required>                                                           
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-4 col-12">
                                <div class="form-group">
                                    <label>Valor</label>
                                    <input class="form-control money2" id="valor" name="valor" placeholder="Digite o valor da conta..." required>                                                           
                                </div>
                            </div>
                        </div>  
                        <div class="row"> 
                        <div class="col-md-12 col-xl-12 col-12">                                
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea class="form-control" rows="3" name="descricao" id="descricao" placeholder="Insira uma descrição..."></textarea>                                                                                           
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg pull-right">Cadastrar</button>                            
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>            
        </div>    
        <div class="col-md-12 col-12">
             <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">Pedidos abertos</h3>              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="mesas" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr class="bg-dark">                                
                                <th style="text-align: center">Nome</th>
                                <th style="text-align: center">Descrição</th>
                                <th style="text-align: center">Data de Vencimento</th>
                                <th style="text-align: center">Data do pagamento</th>
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>                                
                            </tr>
                        </thead>
                        <tbody>	
                            @foreach($contas as $row)                            
                            <tr>       
                                <td style="text-align: center">{{ $row->nome }}</td>
                                <td style="text-align: center">{{ $row->descricao }}</td>  
                                <td style="text-align: center">{{ date( 'd/m/Y', strtotime($row->data_vencimento)) }}</td>  
                                <td style="text-align: center">{{ isset($row->data_pagamento) ? date( 'd/m/Y', strtotime($row->data_pagamento)) : '' }}</td>  
                                <td style="text-align: center"><span class="btn btn-{{ $row->status == 'Pago' ? 'success' : $row->status == 'A vencer' ? 'primary' : 'danger' }} btn-xs">{{ $row->status }}</span></td>
                                <td style="text-align: center">{{ $row->valor }}</td>                                
                                <td style="text-align: center">{{ date( 'd/m/Y', strtotime($row->updated_at)) }}</td>  
                            </tr>                            
                            @endforeach
                        </tbody>				  
                        <tfoot>
                            <tr class="bg-dark">
                                <th style="text-align: center">Nome</th>
                                <th style="text-align: center">Descrição</th>
                                <th style="text-align: center">Data de Vencimento</th>
                                <th style="text-align: center">Data do pagamento</th>
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>   
                            </tr>
                        </tfoot>
                    </table>
                </div>              
            </div>
          </div>
        </div>       
    </div>
      
    </section>

@endsection
@section('page-js')

<!-- InputMask -->
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	
	<!-- date-range-picker -->
	<script src="{{ asset('public/assets/vendor_components/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	
	<!-- bootstrap datepicker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	
	<!-- bootstrap color picker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
	
	<!-- bootstrap time picker -->
	<script src="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
	
	<!-- SlimScroll -->
	<script src="{{ asset('public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- iCheck 1.0.1 -->
	<script src="{{ asset('public/assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
	<script src="{{ asset('public/js/pages/advanced-form-element.js') }}"></script>
	
	<!-- FastClick -->
	<script src="{{ asset('public/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>    
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

        <!-- This is data table -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <!-- start - This is for export functionality only -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('public/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/select2/dist/js/select2.js') }}x"></script>
        <!-- end - This is for export functionality only -->

        <!-- Crypto_Admin for Data Table -->
        <script src="{{ asset('public/js/pages/data-table.js') }}"></script> 
        
        <script src="{{ asset('public/js/jquery.mask.js') }}"></script>
        <script src="{{ asset('public/js/jquery.easy-autocomplete.min.js') }}"></script>
        <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
        $('.date').mask('00/00/0000');
        $('.money2').mask("#.##0.00", {reverse: true});
        $('#mesas').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 8, "asc" ]]
        });           
        </script>
@endsection
