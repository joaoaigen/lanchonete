@extends('layouts.app')

@section('title-head')
Página inicial
@endsection

@section('title-body')
Página inicial
@endsection

@section('page-css')
<link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')
<!-- Main content -->
<section class="content">
    <div class="row"> 
        <div class="col-xl-3 col-md-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info pull-up bg-hexagons-white">
                <div class="inner">
                    <h3>
                        {{ DB::table('pedidos')->where('status', '=', 'Aberto')->count() }}
                    </h3>

                    <p>Pedidos em aberto</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </div>
                <a href="#" class="small-box-footer"><!--<i class="fa fa-arrow-right"></i>--></a>
            </div>
        </div>
        
        <div class="col-xl-3 col-md-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger pull-up bg-hexagons-white">
            <div class="inner">
              <h3>{{ DB::table('pedidos')->where('status', '=', 'Cancelado')->whereDate('created_at', date('Y-m-d'))->count() }}</h3>

              <p>Pedidos cancelados hoje</p>
            </div>
            <div class="icon">
              <i class="fa fa-times"></i>
            </div>
            <a href="#" class="small-box-footer"> <!--<i class="fa fa-arrow-right"></i>--></a>
          </div>
        </div>
        
        <div class="col-xl-3 col-md-3 col-6">
          <!-- small box -->
          <div class="small-box bg-primary pull-up bg-hexagons-white">
            <div class="inner">
              <h3>{{ DB::table('pedidos')->where('status', '=', 'Concluido')->whereDate('created_at', date('Y-m-d'))->count() }}</h3>

              <p>Pedidos concluidos hoje</p>
            </div>
            <div class="icon">
              <i class="fa fa-check"></i>
            </div>
            <a href="#" class="small-box-footer"> <!--<i class="fa fa-arrow-right"></i>--></a>
          </div>
        </div>
        @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
        <div class="col-xl-3 col-md-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success pull-up bg-hexagons-white">
                <div class="inner">
                    <h3>{{ DB::table('pedidos')->where('status', '=', 'Concluido')->whereDate('created_at', date('Y-m-d'))->sum('total') }}</h3>
                    <p>Total vendido hoje</p>
                </div>
                <div class="icon">
                    <i class="fa fa-dollar"></i>
                </div>
                <a href="#" class="small-box-footer"> <!--<i class="fa fa-arrow-right"></i>--></a>
            </div>
        </div>
        @else
        <div class="col-xl-3 col-md-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success pull-up bg-hexagons-white">
                <div class="inner">
                    <h3>{{ DB::table('mesas')->where('status', '=', 'Disponivel')->count() }}</h3>
                    <p>Mesas disponiveis</p>
                </div>
                <div class="icon">
                    <i class="fa fa-thumbs-o-up"></i>
                </div>
                <a href="#" class="small-box-footer"> <!--<i class="fa fa-arrow-right"></i>--></a>
            </div>
        </div>
        @endif
        
        <div class="col-md-12 col-12">
             <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">Pedidos abertos</h3>              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="mesas" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr class="bg-dark">                                
                                <th style="text-align: center">N°</th>
                                <th style="text-align: center">USUÁRIO</th>
                                <th style="text-align: center">PRODUTOS</th>
                                <th style="text-align: center">MESA</th>
                                <th style="text-align: center">TIPO</th>
                                <!--<th style="text-align: center">SITUAÇÃO</th>-->
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>
                                @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <th style="text-align: center">OPÇÕES</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>	
                            @foreach($pedidos as $row)
                            @if($row->status == 'Aberto')
                            <tr>       
                                <td style="text-align: center">{{ $row->id }}</td>
                                <td style="text-align: center">{{ DB::table('users')->where('id', '=', $row->usuario)->first()->username }}</td>
                                <td style="text-align: center">
                                    <?php 
                                       $produtos = json_decode($row->produtos);
                                       foreach($produtos as $row1){
                                           $nome = DB::table('produto')->where('id', '=', $row1->produto)->first()->nome;
                                           $quantidade = $row1->quantidade;
                                           echo $quantidade.'&nbsp;-&nbsp;'.$nome .'&nbsp;&nbsp;  &nbsp;&nbsp;';
                                       }
                                    ?>                                    
                                </td>
                                <td style="text-align: center">{{ isset(DB::table('mesas')->where('id', '=', $row->mesa)->first()->descricao) ? DB::table('mesas')->where('id', '=', $row->mesa)->first()->descricao : '' }}</td>
                                
                                @if($row->tipo == 'Viagem')
                                    <td style="text-align: center"><span class="btn btn-success btn-xs"><i class="fa fa-motorcycle" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                @else
                                    <td style="text-align: center"><span class="btn btn-primary btn-xs"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                @endif
                                
                                <!--@if($row->tipo == 'Viagem')
                                    @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Entrega')
                                        <td style="text-align: center"><span class="btn btn-default btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                @endif-->
                                
                                <!--@if($row->tipo == 'Local')
                                    @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                                              
                                @endif-->
                                <td style="text-align: center"><span class="btn btn-{{ $row->status == 'Aberto' ? 'primary' : $row->status == 'Concluido' ? 'success' : 'danger' }} btn-xs">{{ $row->status }}</span></td>
                                <td style="text-align: center">{{ $row->total }}</td>                                
                                <td style="text-align: center">{{ date( 'd/m/Y H:i:s', strtotime($row->updated_at)) }}</td>     
                               
                                @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master' && $row->status != 'Cancelado')
                                <td style="text-align: center">
                                    <div class="inline-block">
                                        <a class="btn btn-success btn-xs" id="fechar_pedido" href="{{ url('pedidos/fechar-pedido/'.$row->id) }}">FINALIZAR</a> 
                                        <br>
                                        <br>
                                        <a class="btn btn-primary btn-xs" id="editar_pedido" href="{{ url('pedidos/editar-pedido/'.$row->id) }}">EDITAR</a>
                                        <br>
                                        <br>
                                        <a class="btn btn-danger btn-xs" id="cancelar_pedido" href="#" onclick="alerta({{ $row->id }});">CANCELAR</a>
                                    </div>                                    
                                </td>                                
                                @elseif(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <td style="text-align: center"></td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                        </tbody>				  
                        <tfoot>
                            <tr class="bg-dark">
                                <th style="text-align: center">N°</th>
                                <th style="text-align: center">USUÁRIO</th>
                                <th style="text-align: center">PRODUTOS</th>
                                <th style="text-align: center">MESA</th>
                                <th style="text-align: center">TIPO</th>
                               <!--<th style="text-align: center">SITUAÇÃO</th>-->
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>
                                @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <th style="text-align: center">OPÇÕES</th>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </div>              
            </div>
          </div>
        </div>
    </div>
</section>


<!-- /.content -->
@endsection
@section('page-js')

<!-- This is data table -->
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

<!-- start - This is for export functionality only -->
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/js/echarts.min.js') }}"></script>
<script>
    /**
     * Abrir modal de dividir lucro no admin
     */


    $(document).ready(function() {
        
        $('#mesas').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 8, "asc" ]]
        });
        
        @if (isset($errors) && count($errors) > 0)  
        @foreach ($errors->all() as $error)
           swal("Erro!", "{{ $error }}", "error");
        @endforeach            
        @endif
        
        @if(session()->has('status') && session()->get('status') == 200)
        swal("Sucesso!", "{{ session()->get('msg') }}", "success");
        @endif
        @if(session()->has('status') && session()->get('status') == 400)
        swal("Erro!", "{{ session()->get('msg') }}", "error");
        @endif

        $('.valor').maskMoney({
            prefix: '',
            allowNegative: false,
            thousands: '',
            decimal: '.',
            affixesStay: false
        });       

        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');        
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0.00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
            translation: {
                'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                },
                placeholder: "__/__/____"
            }
        });       
    });   
</script>
@endsection

