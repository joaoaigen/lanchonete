@extends('layouts.app')

@section('title-head')
Cadastro de Filial
@endsection

@section('title-body')
Cadastro de Filial
@endsection

@section('page-css')
<!-- daterange picker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

<!-- bootstrap datepicker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/iCheck/all.css') }}">

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/select2/dist/css/select2.min.css') }}">

@endsection
@section('main-content')
<section class="content">

    <div class="row">
        <div class="col-lg-12 col-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de usuários</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                    </div>
                    @endforeach
                    @endif

                    <form action="{{ route('cadastros.filial.store') }}" method="post" class="form-element">
                        @csrf
                        <input hidden name="matriz" value="{{ Auth::user()->empresa }}">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Nome da empresa" name="nome">                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Nome de exibição no sistema" required name="nome_exib">                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cnpj" placeholder="CNPJ" name="cnpj" required>                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control telefone" placeholder="Telefone" name="telefone">                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control celular" placeholder="Celular" name="celular" required>                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cep" onblur="pesquisacep(this.value);" placeholder="CEP" name="cep" id="cep" required>                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Número" name="numero_cep" required>                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Rua" name="rua" id="rua">                        
                        </div>     
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Bairro" readonly name="bairro" id="bairro">                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Cidade" readonly name="cidade" id="cidade">                        
                        </div>      
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="UF" readonly name="uf" id="uf">                        
                        </div>      
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-info btn-block text-uppercase">Cadastrar Empresa</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>            
        </div>    
        
        <div class="col-md-12 col-12">
            <div class="box box-solid bg-dark">
                <div class="box-header with-border">
                    <h3 class="box-title">Lista de empresas cadastradas</h3>              
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="mesas" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr class="bg-dark">
                                    <th style="text-align: center">NOME</th>
                                    <th style="text-align: center">CNPJ</th>
                                    <th style="text-align: center">TELEFONE</th>
                                    <th style="text-align: center">CELULAR</th>
                                    <th style="text-align: center">CEP</th>
                                    <th style="text-align: center">NOME DE EXIBIÇÃO</th>
                                    <th style="text-align: center">STATUS</th>
                                    <th style="text-align: center">MATRIZ/FILIAL</th>
                                    <th style="text-align: center">ATUALIZADO EM</th>
                                </tr>
                            </thead>
                            <tbody>	
                                @foreach($empresa as $row)
                                <tr>
                                    <td style="text-align: center">{{ $row->nome }}</td>
                                    <td style="text-align: center">{{ $row->cnpj }}</td>
                                    <td style="text-align: center">{{ $row->telefone }}</td>
                                    <td style="text-align: center">{{ $row->celular }}</td>
                                    <td style="text-align: center">{{ $row->cep }}</td>
                                    <td style="text-align: center">{{ $row->nome_exib }}</td>
                                    <td style="text-align: center">{{ $row->status }}</td>
                                    <td style="text-align: center">{{ $row->filial == 1 ? 'Filial' : 'Matriz' }}</td>
                                    <td style="text-align: center">{{ date( 'd/m/Y H:i:s', strtotime($row->updated_at)) }}</td>                                   
                                </tr>
                                @endforeach
                                @if(isset($filial))
                                @foreach($filial as $row)
                                <tr>
                                    <td style="text-align: center">{{ $row->nome }}</td>
                                    <td style="text-align: center">{{ $row->cnpj }}</td>
                                    <td style="text-align: center">{{ $row->telefone }}</td>
                                    <td style="text-align: center">{{ $row->celular }}</td>
                                    <td style="text-align: center">{{ $row->cep }}</td>
                                    <td style="text-align: center">{{ $row->nome_exib }}</td>
                                    <td style="text-align: center">{{ $row->status }}</td>
                                    <td style="text-align: center">{{ $row->filial == 1 ? 'Filial' : 'Matriz' }}</td>
                                    <td style="text-align: center">{{ date( 'd/m/Y H:i:s', strtotime($row->updated_at)) }}</td>                                   
                                </tr>
                                @endforeach
                                @endif
                            </tbody>				  
                            <tfoot>
                                <tr class="bg-dark">
                                    <th style="text-align: center">NOME</th>
                                    <th style="text-align: center">CNPJ</th>
                                    <th style="text-align: center">TELEFONE</th>
                                    <th style="text-align: center">CELULAR</th>
                                    <th style="text-align: center">CEP</th>
                                    <th style="text-align: center">NOME DE EXIBIÇÃO</th>
                                    <th style="text-align: center">STATUS</th>
                                    <th style="text-align: center">MATRIZ/FILIAL</th>
                                    <th style="text-align: center">ATUALIZADO EM</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>              
                </div>
            </div>
        </div>
    </div>

</section>

@endsection
@section('page-js')

<!-- InputMask -->
<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<!-- date-range-picker -->
<script src="{{ asset('public/assets/vendor_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<!-- bootstrap color picker -->
<script src="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>

<!-- bootstrap time picker -->
<script src="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ asset('public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- iCheck 1.0.1 -->
<script src="{{ asset('public/assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('public/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>   
<script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

        <!-- This is data table -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <!-- start - This is for export functionality only -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('public/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
        <!-- end - This is for export functionality only -->

        <!-- Crypto_Admin for Data Table -->
        <script src="{{ asset('public/js/pages/data-table.js') }}"></script> 
<script src="{{ asset('public/js/jquery.mask.js') }}"></script>	
<script type="text/javascript">
        $('#mesas').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 0, "desc" ]]
        });
</script>
<script type="text/javascript" >

                        function limpa_formulário_cep() {
                            //Limpa valores do formulário de cep.
                            document.getElementById('rua').value = ("");
                            document.getElementById('bairro').value = ("");
                            document.getElementById('cidade').value = ("");
                            document.getElementById('uf').value = ("");
                        }

                        function meu_callback(conteudo) {
                            if (!("erro" in conteudo)) {
                                //Atualiza os campos com os valores.
                                document.getElementById('rua').value = (conteudo.logradouro);
                                document.getElementById('bairro').value = (conteudo.bairro);
                                document.getElementById('cidade').value = (conteudo.localidade);
                                document.getElementById('uf').value = (conteudo.uf);
                            } //end if.
                            else {
                                //CEP não Encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        }

                        function pesquisacep(valor) {

                            //Nova variável "cep" somente com dígitos.
                            var cep = valor.replace(/\D/g, '');

                            //Verifica se campo cep possui valor informado.
                            if (cep != "") {

                                //Expressão regular para validar o CEP.
                                var validacep = /^[0-9]{8}$/;

                                //Valida o formato do CEP.
                                if (validacep.test(cep)) {

                                    //Preenche os campos com "..." enquanto consulta webservice.
                                    document.getElementById('rua').value = "...";
                                    document.getElementById('bairro').value = "...";
                                    document.getElementById('cidade').value = "...";
                                    document.getElementById('uf').value = "...";

                                    //Cria um elemento javascript.
                                    var script = document.createElement('script');

                                    //Sincroniza com o callback.
                                    script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meu_callback';

                                    //Insere script no documento e carrega o conteúdo.
                                    document.body.appendChild(script);

                                } //end if.
                                else {
                                    //cep é inválido.
                                    limpa_formulário_cep();
                                    alert("Formato de CEP inválido.");
                                }
                            } //end if.
                            else {
                                //cep sem valor, limpa formulário.
                                limpa_formulário_cep();
                            }
                        }
                        ;

</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.celular').mask('(00) 0 0000-0000');
        $('.telefone').mask('(00) 0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
            translation: {
                'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                },
                placeholder: "__/__/____"
            }
        });
        $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
    });
</script>
@endsection
