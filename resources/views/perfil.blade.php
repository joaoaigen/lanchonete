@extends('layouts.app')

@section('title-head')
Perfil do usuário
@endsection

@section('title-body')
Perfil do usuário
@endsection

@section('page-css')

<!-- daterange picker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

<!-- bootstrap datepicker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/iCheck/all.css') }}">

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/select2/dist/css/select2.css') }}">

<link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">

<style type="text/css">
    ::-webkit-input-placeholder {
        color: white;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: white;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: white;  
    }

    :-ms-input-placeholder {  
        color: white;  
    }
</style>

@endsection
@section('main-content')
<section class="content">

    <div class="row">
        <div class="col-lg-12 col-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Preencha as informações abaixo corretamente para atualizar suas informações.</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                    </div>
                    @endforeach
                    @endif
                    
                    <form action="{{ route('user.perfil') }}" method="POST">
                        @csrf
                        <input hidden value="{{ Auth::user()->id }}" name="id">
                        <div class="row">
                            <div class="col-md-6 col-xl-6 col-12">                                
                                <div class="form-group">
                                    <label>Nome de usuário</label>
                                    <input disabled class="form-control" name="username" value="{{ Auth::user()->username }}">                                                                
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-12">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input class="form-control" name="email" value="{{ Auth::user()->email }}" placeholder="Digite um e-mail...">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xl-4 col-12">                                
                                <div class="form-group">
                                    <label>CEP</label>
                                    <input class="form-control cep" id="cep" onblur="pesquisacep(this.value);" name="cep" value="{{ Auth::user()->cep }}" placeholder="Digite um cep...">                                                                
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-4 col-12">
                                <div class="form-group">
                                    <label>Rua</label>
                                    <input class="form-control" id="rua" name="rua" value="{{ Auth::user()->rua }}" placeholder="Digite a rua...">
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-4 col-12">
                                <div class="form-group">
                                    <label>N°</label>
                                    <input class="form-control" name="num_casa" value="{{ Auth::user()->num_casa }}" placeholder="Digite o número da casa...">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xl-4 col-12">                                
                                <div class="form-group">
                                    <label>Bairro</label>
                                    <input class="form-control" id="bairro" name="bairro" value="{{ Auth::user()->bairro }}" placeholder="Digite um bairro...">                                                                
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-4 col-12">
                                <div class="form-group">
                                    <label>Cidade</label>
                                    <input class="form-control" id="cidade" name="cidade" value="{{ Auth::user()->cidade }}" placeholder="Digite a cidade...">
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-4 col-12">
                                <div class="form-group">
                                    <label>UF</label>
                                    <input class="form-control" id="uf" name="uf" value="{{ Auth::user()->uf }}" placeholder="Digite a uf...">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xl-6 col-12">                                
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input class="form-control celular" name="celular" value="{{ Auth::user()->celular }}" placeholder="Digite um número de celular...">                                                                
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-12">
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <input class="form-control telefone" name="telefone" value="{{ Auth::user()->telefone }}" placeholder="Digite um número de telefone...">
                                </div>
                            </div>                            
                        </div>
                                            
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg pull-right">Cadastrar</button>                            
                        </div>
                    </form>
                </div>
            </div>            
        </div>  
    </div>      
    </section>

@endsection
@section('page-js')

<!-- InputMask -->
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	
	<!-- date-range-picker -->
	<script src="{{ asset('public/assets/vendor_components/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	
	<!-- bootstrap datepicker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	
	<!-- bootstrap color picker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
	
	<!-- bootstrap time picker -->
	<script src="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
	
	<!-- SlimScroll -->
	<script src="{{ asset('public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- iCheck 1.0.1 -->
	<script src="{{ asset('public/assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
	<script src="{{ asset('public/js/pages/advanced-form-element.js') }}"></script>
	
	<!-- FastClick -->
	<script src="{{ asset('public/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>    
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

        <!-- This is data table -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <!-- start - This is for export functionality only -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('public/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/select2/dist/js/select2.js') }}x"></script>
        <!-- end - This is for export functionality only -->

        <!-- Crypto_Admin for Data Table -->
        <script src="{{ asset('public/js/pages/data-table.js') }}"></script> 
        
        <script src="{{ asset('public/js/jquery.mask.js') }}"></script>
        <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
       <script type="text/javascript" >           
    
            function limpa_formulário_cep() {
                    //Limpa valores do formulário de cep.
                    document.getElementById('rua').value=("");
                    document.getElementById('bairro').value=("");
                    document.getElementById('cidade').value=("");
                    document.getElementById('uf').value=("");
            }

            function meu_callback(conteudo) {
                if (!("erro" in conteudo)) {
                    //Atualiza os campos com os valores.
                    document.getElementById('rua').value=(conteudo.logradouro);
                    document.getElementById('bairro').value=(conteudo.bairro);
                    document.getElementById('cidade').value=(conteudo.localidade);
                    document.getElementById('uf').value=(conteudo.uf);                    
                } //end if.
                else {
                    //CEP não Encontrado.
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                }
            }
        
            function pesquisacep(valor) {

                //Nova variável "cep" somente com dígitos.
                var cep = valor.replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        document.getElementById('rua').value="...";
                        document.getElementById('bairro').value="...";
                        document.getElementById('cidade').value="...";
                        document.getElementById('uf').value="...";                        

                        //Cria um elemento javascript.
                        var script = document.createElement('script');

                        //Sincroniza com o callback.
                        script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                        //Insere script no documento e carrega o conteúdo.
                        document.body.appendChild(script);

                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            };

        </script>
        
        <script type="text/javascript">
            $(document).ready(function(){
  $('.date').mask('00/00/0000');
  $('.time').mask('00:00:00');
  $('.date_time').mask('00/00/0000 00:00:00');
  $('.cep').mask('00000-000');
  $('.celular').mask('(00) 0 0000-0000');
  $('.telefone').mask('(00) 0000-0000');
  $('.phone_with_ddd').mask('(00) 0000-0000');
  $('.phone_us').mask('(000) 000-0000');
  $('.mixed').mask('AAA 000-S0S');
  $('.cpf').mask('000.000.000-00', {reverse: true});
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  $('.money2').mask("#.##0,00", {reverse: true});
  $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
    translation: {
      'Z': {
        pattern: /[0-9]/, optional: true
      }
    }
  });
  $('.ip_address').mask('099.099.099.099');
  $('.percent').mask('##0,00%', {reverse: true});
  $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
  $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
  $('.fallback').mask("00r00r0000", {
      translation: {
        'r': {
          pattern: /[\/]/,
          fallback: '/'
        },
        placeholder: "__/__/____"
      }
    });
  $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
});
        </script>
@endsection
