<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="public/images/favicon.ico">

        <title>LanchNSystem - Registro </title>

        <!-- Bootstrap 4.0-->
        <link rel="stylesheet" href="public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

        <!-- Bootstrap extend-->
        <link rel="stylesheet" href="public/css/bootstrap-extend.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="public/css/master_style.css">

        <!-- Crypto_Admin skins -->
        <link rel="stylesheet" href="public/css/skins/_all-skins.css">	

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="hold-transition register-page">
        <div class="register-box">

            <div class="register-box-body">
                <div class="register-logo">
                    <a href="{{ route('login') }}"><b>Lanch</b>NSystem</a>
                </div>
                <p class="login-box-msg">Registrar um novo usuário</p>

                <form method="POST" action="{{ route('register') }}" class="form-element">
                    @csrf
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="username" required placeholder="Username">
                        <span class="ion ion-person form-control-feedback "></span>
                        @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="name" placeholder="Nome completo">
                        <span class="ion ion-person form-control-feedback "></span>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" required placeholder="Email">
                        <span class="ion ion-email form-control-feedback "></span>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" required placeholder="Senha">
                        <span class="ion ion-locked form-control-feedback "></span>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password_confirmation" class="form-control" required placeholder="Insira a senha novamente">
                        <span class="ion ion-log-in form-control-feedback "></span>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_1" >
                                <label for="basic_checkbox_1">Eu aceito os <a href="#"><b>Termos</b></a></label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-info btn-block margin-top-10">Registrar</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                {{-- <div class="social-auth-links text-center">
                    <p>- OU -</p>
                    <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
                </div> --}}
                <!-- /.social-auth-links -->

                <div class="margin-top-20 text-center">
                    <p>Já tem uma conta?<a href="{{ route('login') }}" class="text-info m-l-5"> Fazer login</a></p>
                </div>

            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->


        <!-- jQuery 3 -->
        <script src="{{ asset('assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

        <!-- popper -->
        <script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>

        <!-- Bootstrap 4.0-->
        <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>


    </body>
</html>
