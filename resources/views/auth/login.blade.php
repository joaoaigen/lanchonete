<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('img/favicon.png') }}">

    <title>Wintrom - Fazer login </title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('login') }}"><b>Wintrom</b> Gestor</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sejam bem vindo! Efetue o login para continuar. Caso não tenha uma conta clique no botão registrar-se.</p>
        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Ops!</strong> Houve alguns problemas com a sua entrada.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{ route('login') }}" method="post" class="form-element">
            @csrf
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" placeholder="Nome de usuário">
                <span class="ion ion-person form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Insira sua senha">
                <span class="ion ion-locked form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="checkbox">
                        <input type="checkbox" id="basic_checkbox_1" >
                        <label for="basic_checkbox_1">Lembre-se</label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-6">
                    <div class="fog-pwd">
                        <a href="{{ route('password.request') }}"><i class="ion ion-locked"></i> Perdeu a senha?</a><br>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-info btn-block margin-top-10">Entrar</button>
                    <a href="{{ route('register') }}" class="btn btn-default btn-block margin-top-10">Registrar-se</a>
                </div>
                <!-- /.col -->
            </div>
        </form>        
        <!-- /.social-auth-links -->

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<!-- jQuery 3 -->
<script src="{{ asset('public/assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

<!-- popper -->
<script src="{{ asset('public/assets/vendor_components/popper/dist/popper.min.js') }}"></script>

<!-- Bootstrap 4.0-->
<script src="{{ asset('public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript">
@if(session()->has('status') && session()->get('status') == 200)
swal("Sucesso!", "{{ session()->get('msg') }}", "success");
@endif
@if(session()->has('status') && session()->get('status') == 400)
swal("Erro!", "{{ session()->get('msg') }}", "error");
@endif
</script>
</body>
</html>
