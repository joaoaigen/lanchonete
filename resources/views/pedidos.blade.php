@extends('layouts.app')

@section('title-head')
Cadastrar um pedido
@endsection

@section('title-body')
Cadastrar um pedido
@endsection

@section('page-css')

<!-- daterange picker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

<!-- bootstrap datepicker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/iCheck/all.css') }}">

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/select2/dist/css/select2.css') }}">

<link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">

<style type="text/css">
    ::-webkit-input-placeholder {
        color: white;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: white;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: white;  
    }

    :-ms-input-placeholder {  
        color: white;  
    }
</style>

@endsection
@section('main-content')
<section class="content">

    <div class="row">
        <div class="col-lg-12 col-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Preencha as informações abaixo corretamente para cadastrar</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                    </div>
                    @endforeach
                    @endif
                    
                    <form action="{{ route('cadastros.pedidos.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 col-xl-6 col-12">                                
                                <div class="form-group">
                                    <label>Mesa</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                        </div>
                                        <select class="form-control" name="mesa" required>
                                            <option value="0" selected>Selecione uma mesa</option>
                                            @foreach($mesas as $row)
                                                @if($row->status == 'Disponivel')                                                
                                                    <option value="{{ $row->id }}">{{ 'Mesa: ' . $row->numero }}</option>
                                                @endif
                                            @endforeach                                            
                                        </select>
                                    </div>                            
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-12">
                                <div class="form-group">
                                    <label>Usuário</label>
                                    <input class="form-control" onclick="$('#cadastrar').prop('disabled', false)" id="usuarios" name="usuario" required>                                                              
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Produtos</label>
                            <select class="form-control select2 w-p100" style="color: #252525" multiple="multiple" id="produtos" data-placeholder="Selecione o produto" name="produtos[]" required> 
                                @foreach($produtos as $row)
                                <option value="{{ $row->id }}" id="{{ ' '. $row->id .' ' }}">{{ $row->nome }}</option>
                                @endforeach
                            </select>  
                            
                            <input hidden id="prod" value="{{$id_produtos}}">
                        </div>
                        
                        <div class="row" id="addProdutos">
                            
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6 col-xl-6 col-12">                                
                                <div class="form-group">
                                    <label>Tipo</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                        </div>
                                        <select class="form-control" name="tipo" required>                                             
                                            <option value="Local">Local</option>                                         
                                            <option value="Viagem">Viagem</option>
                                        </select>
                                    </div>                            
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-12">
                                <div class="form-group">
                                    <label>Total</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-money"></i>
                                            </div>
                                        </div>
                                        <input class="form-control money2" id="total" name="total" required>                                   
                                    </div>                            
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" disabled id="cadastrar" onclick="$('#cadastrar').prop('disabled', true)" class="btn btn-primary btn-lg pull-right">Cadastrar</button>                            
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>            
        </div>    
        <div class="col-md-12 col-12">
             <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">Pedidos abertos</h3>              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="mesas" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr class="bg-dark">                                
                                <th style="text-align: center">N°</th>
                                <th style="text-align: center">USUÁRIO</th>
                                <th style="text-align: center">PRODUTOS</th>
                                <th style="text-align: center">MESA</th>
                                <th style="text-align: center">TIPO</th>
                                <!--<th style="text-align: center">SITUAÇÃO</th>-->
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>
                                @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <th style="text-align: center">OPÇÕES</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>	
                            @foreach($pedidos as $row)
                            @if($row->status == 'Aberto')
                            <tr>       
                                <td style="text-align: center">{{ $row->id }}</td>
                                <td style="text-align: center">{{ DB::table('users')->where('id', '=', $row->usuario)->first()->username }}</td>
                                <td style="text-align: center">
                                    <?php 
                                       $produtos = json_decode($row->produtos);
                                       foreach($produtos as $row1){
                                           $nome = DB::table('produto')->where('id', '=', $row1->produto)->first()->nome;
                                           $quantidade = $row1->quantidade;
                                           echo $quantidade.'&nbsp;-&nbsp;'.$nome .'&nbsp;&nbsp;  &nbsp;&nbsp;';
                                       }
                                    ?>                                    
                                </td>
                                <td style="text-align: center">{{ isset(DB::table('mesas')->where('id', '=', $row->mesa)->first()->descricao) ? DB::table('mesas')->where('id', '=', $row->mesa)->first()->descricao : '' }}</td>
                                
                                @if($row->tipo == 'Viagem')
                                    <td style="text-align: center"><span class="btn btn-success btn-xs"><i class="fa fa-motorcycle" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                @else
                                    <td style="text-align: center"><span class="btn btn-primary btn-xs"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                @endif
                                
                                <!--@if($row->tipo == 'Viagem')
                                    @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Entrega')
                                        <td style="text-align: center"><span class="btn btn-default btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                @endif-->
                                
                                <!--@if($row->tipo == 'Local')
                                    @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                                              
                                @endif-->
                                <td style="text-align: center"><span class="btn btn-{{ $row->status == 'Aberto' ? 'primary' : $row->status == 'Concluido' ? 'success' : 'danger' }} btn-xs">{{ $row->status }}</span></td>
                                <td style="text-align: center">{{ $row->total }}</td>                                
                                <td style="text-align: center">{{ date( 'd/m/Y H:i:s', strtotime($row->updated_at)) }}</td>     
                               
                                @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master' && $row->status != 'Cancelado')
                                <td style="text-align: center">
                                    <div class="inline-block">
                                        <a class="btn btn-success btn-xs" id="fechar_pedido" href="{{ url('pedidos/fechar-pedido/'.$row->id) }}">FINALIZAR</a> 
                                        <br>
                                        <br>
                                        <a class="btn btn-primary btn-xs" id="editar_pedido" href="{{ url('pedidos/editar-pedido/'.$row->id) }}">EDITAR</a>
                                        <br>
                                        <br>
                                        <a class="btn btn-danger btn-xs" id="cancelar_pedido" href="#" onclick="alerta({{ $row->id }});">CANCELAR</a>
                                    </div>                                    
                                </td>                                
                                @elseif(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <td style="text-align: center"></td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                        </tbody>				  
                        <tfoot>
                            <tr class="bg-dark">
                                <th style="text-align: center">N°</th>
                                <th style="text-align: center">USUÁRIO</th>
                                <th style="text-align: center">PRODUTOS</th>
                                <th style="text-align: center">MESA</th>
                                <th style="text-align: center">TIPO</th>
                               <!--<th style="text-align: center">SITUAÇÃO</th>-->
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>
                                @if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <th style="text-align: center">OPÇÕES</th>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </div>              
            </div>
          </div>
        </div>
        <div class="col-md-12 col-12">
             <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">Lista de pedidos</h3>              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="mesas" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr class="bg-dark">
                                <th style="text-align: center">EMPRESA</th>
                                <th style="text-align: center">USUÁRIO</th>
                                <th style="text-align: center">PRODUTOS</th>
                                <th style="text-align: center">MESA</th>
                                <th style="text-align: center">TIPO</th>
                                <!--<th style="text-align: center">SITUAÇÃO</th>-->
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>
                                <!--@if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <th style="text-align: center">OPÇÕES</th>
                                @endif-->
                            </tr>
                        </thead>
                        <tbody>	
                            @foreach($pedidos as $row)
                            @if($row->status != 'Aberto')
                            <tr>                                
                                <td style="text-align: center">{{ DB::table('empresas')->where('id', '=', $row->empresa)->first()->nome_exib }}</td>
                                <td style="text-align: center">{{ DB::table('users')->where('id', '=', $row->usuario)->first()->username }}</td>
                                <td style="text-align: center">
                                    <?php
                                    $produtos = json_decode($row->produtos);
                                    foreach ($produtos as $row1) {
                                        $nome = DB::table('produto')->where('id', '=', $row1->produto)->first()->nome;
                                        $quantidade = $row1->quantidade;
                                        echo $quantidade.'&nbsp;-&nbsp;'.$nome .'&nbsp;&nbsp;  &nbsp;&nbsp;';
                                    }
                                    ?> 
                                </td>
                                <td style="text-align: center">{{ isset(DB::table('mesas')->where('id', '=', $row->mesa)->first()->descricao) ? DB::table('mesas')->where('id', '=', $row->mesa)->first()->descricao : '' }}</td>
                                
                                @if($row->tipo == 'Viagem')
                                    <td style="text-align: center"><span class="btn btn-success btn-xs"><i class="fa fa-motorcycle" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                @else
                                    <td style="text-align: center"><span class="btn btn-primary btn-xs"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; {{ $row->tipo }}</span></td>
                                @endif
                                
                                <!--@if($row->tipo == 'Viagem')
                                    @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Entrega')
                                        <td style="text-align: center"><span class="btn btn-default btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_viagem }}</span></td>  
                                    @endif
                                @endif-->
                                
                                <!--@if($row->tipo == 'Local')
                                    @if($row->status_local == 'Gerado')
                                        <td style="text-align: center"><span class="btn btn-primary btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Preparando')
                                        <td style="text-align: center"><span class="btn btn-warning btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                    @if($row->status_local == 'Concluido')
                                        <td style="text-align: center"><span class="btn btn-success btn-xs">{{ $row->status_local }}</span></td>  
                                    @endif
                                                              
                                @endif-->
                                <td style="text-align: center"><span class="btn btn-{{ $row->status == 'Aberto' ? 'primary' : $row->status == 'Concluido' ? 'success' : 'danger' }} btn-xs">{{ $row->status }}</span></td>
                                <td style="text-align: center">{{ $row->total }}</td>                                
                                <td style="text-align: center">{{ date( 'd/m/Y H:i:s', strtotime($row->updated_at)) }}</td>     
                               
                                <!--@if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master' && $row->status != 'Cancelado')                                
                                <td style="text-align: center"><a class="btn btn-danger btn-xs" id="cancelar_pedido" href="#" onclick="alerta({{ $row->id }});">CANCELAR</a></td>
                                @elseif(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <td style="text-align: center"></td>
                                @endif-->
                            </tr>
                            @endif
                            @endforeach
                        </tbody>				  
                        <tfoot>
                            <tr class="bg-dark">
                                <th style="text-align: center">EMPRESA</th>
                                <th style="text-align: center">USUÁRIO</th>
                                <th style="text-align: center">PRODUTOS</th>
                                <th style="text-align: center">MESA</th>
                                <th style="text-align: center">TIPO</th>
                                <!--<th style="text-align: center">SITUAÇÃO</th>-->
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">TOTAL</th>
                                <th style="text-align: center">ATUALIZADO EM</th>
                                <!--@if(Auth::user()->tipo == 'Administrador' or Auth::user()->tipo == 'Master')
                                <th style="text-align: center">OPÇÕES</th>
                                @endif-->
                            </tr>
                        </tfoot>
                    </table>
                </div>              
            </div>
          </div>
        </div>
    </div>
      
    </section>

@endsection
@section('page-js')

<!-- InputMask -->
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	
	<!-- date-range-picker -->
	<script src="{{ asset('public/assets/vendor_components/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	
	<!-- bootstrap datepicker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	
	<!-- bootstrap color picker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
	
	<!-- bootstrap time picker -->
	<script src="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
	
	<!-- SlimScroll -->
	<script src="{{ asset('public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- iCheck 1.0.1 -->
	<script src="{{ asset('public/assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
	<script src="{{ asset('public/js/pages/advanced-form-element.js') }}"></script>
	
	<!-- FastClick -->
	<script src="{{ asset('public/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>    
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

        <!-- This is data table -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <!-- start - This is for export functionality only -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('public/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/select2/dist/js/select2.js') }}x"></script>
        <!-- end - This is for export functionality only -->

        <!-- Crypto_Admin for Data Table -->
        <script src="{{ asset('public/js/pages/data-table.js') }}"></script> 
        
        <script src="{{ asset('public/js/jquery.mask.js') }}"></script>
        <script src="{{ asset('public/js/jquery.easy-autocomplete.min.js') }}"></script>
        <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
        function alerta(id){
            swal({
                title: "Você tem certeza?",
                text: "Ao cancelar esse pedido, tudo relacionado a ele será cancelado junto!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location.href = 'http://lanchonete.nsystemsolutions.com.br/pedidos/cancelar-pedido/' + id;
                } else {
                    swal("Pedido de cancelamento abortado!");
                }
            });
        }
        $('#mesas').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 8, "asc" ]]
        });
        
        function somaProdutos(id){
            $("#" + id).mask('0000000000');
            var prod = $('#produtos').val();            
            var qntd = $("#" + id).val();            
            var arr = new Array();            
            var i = 0;
            $.each( prod, function( key, value ) { 
                if($("#input" + value).length > 0){
                   arr[i] = {'produto': value, 'qntd': $("#input" + value).val()};
                    i++; 
                }                
            });            
            
            $.ajax({
                url : "/pedidos/calcular-pedido",
                type : 'get',
                data : {produtos: arr },
            beforeSend : function(){
                console.log('Enviando...')
              }
            })
            .done(function(msg){                
                $('#total').val(msg);
            })
            .fail(function(jqXHR, textStatus, msg){
                
            });
        }
        
        function ajustePreco(){
            var prod = $('#produtos').val();
            var arr = new Array();            
            var i = 0;
            
            $.each( prod, function( key, value ) { 
                if($("#input" + value).length > 0){
                   arr[i] = {'produto': value, 'qntd': $("#input" + value).val()};
                   i++;                   
                }                
            });
            
            $.ajax({
                url : "/pedidos/calcular-pedido",
                type : 'get',
                data : {produtos: arr },
            beforeSend : function(){
                console.log('Enviando...')
              }
            })
            .done(function(msg){                
                $('#total').val(msg);
            })
            .fail(function(jqXHR, textStatus, msg){
                
            });
            
        }
        </script>
        
        <script type="text/javascript">            
        $(document).ready(function(){
            
            var options = {
                    url: "/lista-usuarios",

                    getValue: "username",

                    list: {	
                        match: {
                            enabled: true
                        }
                    },

                    theme: "square"
            };

            $("#usuarios").easyAutocomplete(options);
            
         $( "#produtos" ).change(function() {             
            var prod = $('#produtos').val();
            
            $.each( prod, function( key, value ) {                
                
                if ($(".fechar" + value).length){   
                   
                } else {
                    $.ajax({
                        url : "/produtos/pesquisa",
                        type : 'get',
                        data : {id: value},
                    beforeSend : function(){
                        $("#produtos").prop('disabled', true);
                      }
                    })
                    .done(function(msg){
                        $('#addProdutos').append("<div class='col-md-1 col-xl-2 col-6 fechar"+ value +"' > <div class='form-group'> <label>QNTD. " + msg.nome + "</label> <input style='border-color: green' class='form-control num' value='1'  name='"+ value +"' id='input"+ value +"' onkeyup='somaProdutos(this.id)' required>  </div> <button type='button' class='btn btn-danger btn-xs' onclick=$('.fechar" +  value +"').remove();ajustePreco();  >EXCLUIR "+ msg.nome +"</button><br></div>");
                        $('#addProdutos').append("<div class='col-md-1 col-xl-2 col-6 fechar"+ value +"' > <div class='form-group'> <label>DESC. " + msg.nome + "</label> <input style='border-color: blue' class='form-control'  name='desc"+ value +"'>  </div><br></div>");
                        $("#produtos").prop('disabled', false);
                        ajustePreco();
                    })
                    .fail(function(jqXHR, textStatus, msg){
                        swal(msg, textStatus);
                    });
                }                 
                 
            });            
            
         });
            
        @if(session()->has('status') && session()->get('status') == 200)
        swal("Sucesso!", "{{ session()->get('msg') }}", "success");
        @endif
        @if(session()->has('status') && session()->get('status') == 400)
        swal("Erro!", "{{ session()->get('msg') }}", "error");
        @endif

        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.num').mask('0000000000');
        $('.celular').mask('(00) 0 0000-0000');
        $('.telefone').mask('(00) 0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0.00", {reverse: true});
        $('.quantidade').mask("#.##0.000", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
        translation: {
        'Z': {
        pattern: /[0-9]/, optional: true
        }
        }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
        translation: {
        'r': {
        pattern: /[\/]/,
        fallback: '/'
        },
        placeholder: "__/__/____"
        }
        });
        $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
        </script>
@endsection
