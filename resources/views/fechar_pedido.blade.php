@extends('layouts.app')

@section('title-head')
Finalizar pedido
@endsection

@section('title-body')
Finalizar pedido
@endsection

@section('page-css')

<!-- daterange picker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

<!-- bootstrap datepicker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/iCheck/all.css') }}">

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/select2/dist/css/select2.css') }}">

<link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">

<style type="text/css">
    ::-webkit-input-placeholder {
        color: white;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: white;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: white;  
    }

    :-ms-input-placeholder {  
        color: white;  
    }
</style>

@endsection
@section('main-content')
<section class="content">
    <div class="row">
        <div class="col-lg-12 col-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Confira as informações abaixo para finalizar o pedido</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                    </div>
                    @endforeach
                    @endif

                    <form action="{{ route('fechar.pedido') }}" id="fecharPedido" method="POST">
                        @csrf
                        <input hidden name="id" value="{{ $dados->id }}">
                        <input hidden name="confirmar" value="true">
                        <div class="row">
                            <div class="col-md-6 col-xl-6 col-12">                                
                                <div class="form-group">
                                    <label>Mesa</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                        </div>
                                        <select class="form-control" name="mesa" required>                                            
                                            @foreach($mesas as $row)
                                            @if($row->id == $dados->mesa)                                            
                                            <option selected value="{{ $row->id }}">{{ strtoupper('Mesa: ') . $row->numero }}</option>                                            
                                            @endif
                                            @endforeach                                            
                                        </select>
                                    </div>                            
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-12">
                                <div class="form-group">
                                    <label>Usuário</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-user"></i>
                                            </div>
                                        </div>
                                        <select class="form-control" name="usuario" required> 
                                            @foreach($usuario as $row)
                                            @if($row->id == $dados->usuario)                                            
                                            <option selected value="{{ $row->id }}">{{ strtoupper($row->username) }}</option>   
                                            @else
                                            <option selected value="{{ $row->id }}">{{ strtoupper($row->username) }}</option>
                                            @endif
                                            @endforeach
                                        </select>                                     
                                    </div>                            
                                </div>
                            </div>
                        </div>                        

                        <div class="row">
                            <div class="col-md-6 col-xl-6 col-12">                                
                                <div class="form-group">
                                    <label>Tipo</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                        </div>
                                        <select class="form-control" name="tipo" required>  
                                            @if($dados->tipo == 'Local')
                                            <option selected value="Local">LOCAL</option>   
                                            @else
                                            <option selected value="Viagem">VIAGEM</option>
                                            @endif
                                        </select>
                                    </div>                            
                                </div>
                            </div>                            
                        </div>  

                        <br>
                        <h3>Produtos consumidos</h3>
                        <hr>

                        <div class="table-responsive">
                            <table class="table table-hover b-2">
                                <thead>
                                    <tr class="bg-dark">
                                        <th style="text-align: center">CHECK</th>
                                        <th style="text-align: center">QUANTIDADE</th>                                        
                                        <th style="text-align: center">PRODUTO</th>                                        
                                    </tr>
                                </thead>
                                <tbody>  
                                    @foreach(json_decode($dados->produtos) as $row)
                                    <tr>

                                        <td style="text-align: center">
                                            <div class="form-group ichack-input">                                            
                                                <label>
                                                    <input type="checkbox" class="minimal-red" name="{{ $row->produto }}" id="{{ $row->produto }}" value="{{ $row->quantidade }}">                                               
                                                </label>
                                            </div>
                                        </td>                                        
                                        <td style="text-align: center" class="d-flex justify-content-center mailbox-name">
                                            <div class="col-md-2 col-3">
                                                <div class="input-group">                                                    
                                                    <input type="text" class="form-control text-center num" name="qntd{{ $row->produto }}" onkeyup="$('#{{ $row->produto }}').val(this.value)" value="{{ $row->quantidade }}">                                     
                                                </div>  
                                            </div>
                                        </td>
                                        <td style="text-align: center" class="mailbox-name">{{ strtoupper(DB::table('produto')->where('id', '=', $row->produto)->first()->nome) }}</td>
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>                                          
                        <br>                         
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-lg pull-right" id="finaPedido">Finalizar</button>                            
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>            
        </div>          
    </div>      
</section>


<div class="modal modal-right fade" id="finalizarPedido" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmar fechamento</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>
                    Confirme as informações do pedido para concluir.
                </h6><br>
                
                <form method="POST" action="{{ route('fechar.pedido') }}" id="fechar_pedido">
                    @csrf
                    <input hidden id="pedido" name="pedido">                    
                    <div class="col-md-12 col-xl-12 col-12">
                        <div class="form-group">
                            <label> Mesa </label>
                            <div class="input-group">                                
                                <input class="form-control" readonly type="text" name="mesa" id="mesa">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12 col-12">
                        <div class="form-group">
                            <label>Usuário </label>
                            <div class="input-group">                                
                                <input class="form-control" type="text" name="usuario" id="usuario">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12 col-12">
                        <div class="form-group">
                            <label>Forma de pagamento </label>
                            <div class="input-group">                                
                                <select class="form-control" name="forma_pagamento" id="forma-pagamento" required> 
                                    <option value="0">Dinheiro</option>
                                    <option value="1">Cartão de Débito</option>
                                    <option value="2">Cartão de Crédito</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12 col-12">
                        <div class="form-group">
                            <label>Total</label>
                            <div class="input-group">                                
                                <input class="form-control money2" type="text" name="total" id="total">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div id="tabelaProdutos">
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer modal-footer-uniform">
                <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-bold btn-pure btn-primary float-right" onclick="$('#fechar_pedido').submit()">Concluir</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')

<!-- InputMask -->
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	
	<!-- date-range-picker -->
	<script src="{{ asset('public/assets/vendor_components/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	
	<!-- bootstrap datepicker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	
	<!-- bootstrap color picker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
	
	<!-- bootstrap time picker -->
	<script src="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
	
	<!-- SlimScroll -->
	<script src="{{ asset('public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- iCheck 1.0.1 -->
	<script src="{{ asset('public/assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
	<script src="{{ asset('public/js/pages/advanced-form-element.js') }}"></script>
	
	<!-- FastClick -->
	<script src="{{ asset('public/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>    
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

        <!-- This is data table -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <!-- start - This is for export functionality only -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('public/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/select2/dist/js/select2.js') }}x"></script>
        <!-- end - This is for export functionality only -->

        <!-- Crypto_Admin for Data Table -->
        <script src="{{ asset('public/js/pages/data-table.js') }}"></script> 
        
        <script src="{{ asset('public/js/jquery.mask.js') }}"></script>
        <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
        function alerta(id){
            swal({
                title: "Você tem certeza?",
                text: "Ao cancelar esse pedido, tudo relacionado a ele será cancelado junto!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location.href = 'http://lanchonete.nsystemsolutions.com.br/pedidos/cancelar-pedido/' + id;
                } else {
                    swal("Pedido de cancelamento abortado!");
                }
            });
        }
        $('#mesas').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 8, "asc" ]]
        });
        
        function somaProdutos(id){
            console.log(id);
            
           /* $("#" + id).mask('0000000000');
            var prod = $('#produtos').val();            
            var qntd = $("#" + id).val();            
            var arr = new Array();            
            var i = 0;
            $.each( prod, function( key, value ) { 
                if($("#input" + value).length > 0){
                   arr[i] = {'produto': value, 'qntd': $("#input" + value).val()};
                    i++; 
                }                
            });            
            
            $.ajax({
                url : "/pedidos/calcular-pedido",
                type : 'get',
                data : {produtos: arr },
            beforeSend : function(){
                console.log('Enviando...')
              }
            })
            .done(function(msg){                
                $('#total').val(msg);
            })
            .fail(function(jqXHR, textStatus, msg){
                
            });*/
        }       
       
        </script>
        
        <script type="text/javascript">            
        $(document).ready(function(){   
                        
         $( "#finaPedido" ).click(function() {             
             var form = $('#fecharPedido').serialize();
             
             $.ajax({
                url : '{{ route('fechar.pedido') }}',
                type : 'post',
                data : form,
             beforeSend : function(){
                console.log('Enviando...')
              }
            })
            .done(function(msg){   
                $('#tabelaProdutos').empty();                
                $('#finalizarPedido').modal('show');
                $('#mesa').val(msg.mesa);
                $('#usuario').val(msg.usuario);
                $('#total').val(msg.total);
                $('#pedido').val(msg.pedido);  
                $('#produtos').val(msg.produtos);
                
                $.each( msg.produtos, function( key, value ) { 
                    $('#tabelaProdutos').append( value.quantidade + ' &ensp; ' + value.produto + ' &ensp; '+ value.valor + '<br>'); 
                    $('#tabelaProdutos').append("<input hidden name='" + value.id + "' value='" + value.quantidade + "'>"); 
                });
            })
            .fail(function(jqXHR, textStatus, msg){
                
            });            
         });
            
        @if(session()->has('status') && session()->get('status') == 200)
        swal("Sucesso!", "{{ session()->get('msg') }}", "success");
        @endif
        @if(session()->has('status') && session()->get('status') == 400)
        swal("Erro!", "{{ session()->get('msg') }}", "error");
        @endif

        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.num').mask('0000000000');
        $('.celular').mask('(00) 0 0000-0000');
        $('.telefone').mask('(00) 0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0.00", {reverse: true});
        $('.quantidade').mask("#.##0.000", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
        translation: {
        'Z': {
        pattern: /[0-9]/, optional: true
        }
        }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
        translation: {
        'r': {
        pattern: /[\/]/,
        fallback: '/'
        },
        placeholder: "__/__/____"
        }
        });
        $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
        </script>
@endsection
