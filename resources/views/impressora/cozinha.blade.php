<?php 
    dd($data);
?>
<!DOCTYPE html>
<html lang="pt-br" style="width: 80mm">
<head>  
    <meta charset="utf-8">
    <title>{{  }} &middot; Impressão em PDV</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Teste de Impressão com PDV">
    <meta name="author" content="Guilherme Hahn">
    <link href="{{ asset('public/css/print.css') }}" rel="stylesheet"  type="text/css"  media="print,screen"/>
    <script type="text/javascript">  
                  
    </script>
</head>

<body>
    <div id="printable" style="width:90mm;" align="center">
        
        <h1>MESA:&nbsp; </h1>
        <hr>
        <h2>Texto</h2>
        <h3>Texto Qualquer</h3>
        <h3>Texto Qualquer</h3>
        <h3>Texto Qualquer</h3>
        <h3>Texto Qualquer</h3>
        <h3>Texto Qualquer</h3>
        <h5>Emitido em <?php echo date('d/m/Y') . ' às ' . date('H') . 'h' . date('i') . 'm' . date('s') . 's'; ?></h5>
        <hr>
    </div>
</body>
</html>