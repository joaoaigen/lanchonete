@extends('layouts.app')

@section('title-head')
Editar pedido
@endsection

@section('title-body')
Editar pedido
@endsection

@section('page-css')

<!-- daterange picker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/bootstrap-extend.css') }}">

<!-- bootstrap datepicker -->	
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/iCheck/all.css') }}">

<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/assets/vendor_components/select2/dist/css/select2.css') }}">

<link rel="stylesheet" href="{{ asset('public/css/master_style.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/skins/_all-skins.css') }}">

<style type="text/css">
    ::-webkit-input-placeholder {
        color: white;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: white;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: white;  
    }

    :-ms-input-placeholder {  
        color: white;  
    }
</style>

@endsection
@section('main-content')
<section class="content">

    <div class="row">
        <div class="col-lg-12 col-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Preencha as informações abaixo corretamente para cadastrar</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  {{ $error == 'O campo username já está sendo utilizado.' ? 'Nome de usuário já existe.' : $error }}
                    </div>
                    @endforeach
                    @endif
                    
                    <form action="{{ route('pedidos.editar') }}" method="POST">
                        @csrf
                        <input hidden value="{{ $id }}" name="id">
                        <div class="row">
                            <div class="col-md-6 col-xl-6 col-12">                                
                                <div class="form-group">
                                    <label>Mesa</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                        </div>
                                        <select class="form-control" name="mesa" required>                                            
                                            <option selected value="{{ $dados->mesa }}"> {{ isset(DB::table('mesas')->where('id', $dados->mesa)->first()->numero) ?  DB::table('mesas')->where('id', $dados->mesa)->first()->numero : 'SEM MESA'}}</option>                                          
                                        </select>
                                    </div>                            
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-12">
                                <div class="form-group">
                                    <label>Usuário</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-user"></i>
                                            </div>
                                        </div>
                                        <select class="form-control" name="usuario" required>                                             
                                            <option selected value="{{ $dados->usuario }}">{{ DB::table('users')->where('id', $dados->usuario)->first()->username }}</option>                                            
                                        </select>                                     
                                    </div>                            
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Produtos</label>
                            <select class="form-control select2 w-p100" style="color: #252525" multiple="multiple" id="produtos" data-placeholder="Selecione o produto" name="produtos[]" required> 
                                @foreach($produtos as $row)
                                    <option value="{{ $row->id }}" id="{{ ' '. $row->id .' ' }}">{{ $row->nome }}</option>
                                @endforeach
                            </select>  
                            
                            <input hidden id="prod" value="{{$id_produtos}}">
                        </div>
                        
                        <div class="row" id="addProdutos">
                            
                        </div>                       
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg pull-right">Cadastrar</button>                            
                        </div>
                    </form>
                </div>
            </div>            
        </div>  
    </div>      
    </section>

@endsection
@section('page-js')

<!-- InputMask -->
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	
	<!-- date-range-picker -->
	<script src="{{ asset('public/assets/vendor_components/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	
	<!-- bootstrap datepicker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	
	<!-- bootstrap color picker -->
	<script src="{{ asset('public/assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
	
	<!-- bootstrap time picker -->
	<script src="{{ asset('public/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
	
	<!-- SlimScroll -->
	<script src="{{ asset('public/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- iCheck 1.0.1 -->
	<script src="{{ asset('public/assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
	<script src="{{ asset('public/js/pages/advanced-form-element.js') }}"></script>
	
	<!-- FastClick -->
	<script src="{{ asset('public/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>    
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

        <!-- This is data table -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <!-- start - This is for export functionality only -->
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('public/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor_components/select2/dist/js/select2.js') }}x"></script>
        <!-- end - This is for export functionality only -->

        <!-- Crypto_Admin for Data Table -->
        <script src="{{ asset('public/js/pages/data-table.js') }}"></script> 
        
        <script src="{{ asset('public/js/jquery.mask.js') }}"></script>
        <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
        <script type="text/javascript">        
                
        function somaProdutos(id){
            $("#" + id).mask('0000000000');
            var prod = $('#produtos').val();            
            var qntd = $("#" + id).val();            
            var arr = new Array();            
            var i = 0;
            $.each( prod, function( key, value ) { 
                if($("#input" + value).length > 0){
                   arr[i] = {'produto': value, 'qntd': $("#input" + value).val()};
                    i++; 
                }                
            });            
            
            $.ajax({
                url : "/pedidos/calcular-pedido",
                type : 'get',
                data : {produtos: arr },
            beforeSend : function(){
                console.log('Enviando...')
              }
            })
            .done(function(msg){                
                $('#total').val(msg);
            })
            .fail(function(jqXHR, textStatus, msg){
                
            });
        }
        
        function ajustePreco(){
            var prod = $('#produtos').val();
            var arr = new Array();            
            var i = 0;
            
            $.each( prod, function( key, value ) { 
                if($("#input" + value).length > 0){
                   arr[i] = {'produto': value, 'qntd': $("#input" + value).val()};
                   i++;                   
                }                
            });
            
            $.ajax({
                url : "/pedidos/calcular-pedido",
                type : 'get',
                data : {produtos: arr },
            beforeSend : function(){
                console.log('Enviando...')
              }
            })
            .done(function(msg){                
                $('#total').val(msg);
            })
            .fail(function(jqXHR, textStatus, msg){
                
            });
            
        }
        </script>
        
        <script type="text/javascript">            
        $(document).ready(function(){             
            
         $( "#produtos" ).change(function() {
            var prod = $('#produtos').val();
            
            $.each( prod, function( key, value ) {                
                
                if ($(".fechar" + value).length){   
                   
                } else {
                    $.ajax({
                        url : "/produtos/pesquisa",
                        type : 'get',
                        data : {id: value},
                    beforeSend : function(){
                        $("#produtos").prop('disabled', true);
                      }
                    })
                    .done(function(msg){
                        $('#addProdutos').append("<div class='col-md-1 col-xl-2 col-6 fechar"+ value +"' > <div class='form-group'> <label>QNTD. " + msg.nome + "</label> <input style='border-color: green' class='form-control num' value='1' name='"+ value +"' id='input"+ value +"' required>  </div> <button type='button' class='btn btn-danger btn-xs' onclick=$('.fechar" +  value +"').remove();>EXCLUIR "+ msg.nome +"</button><br></div>");
                        $('#addProdutos').append("<div class='col-md-1 col-xl-2 col-6 fechar"+ value +"' > <div class='form-group'> <label>DESC. " + msg.nome + "</label> <input style='border-color: blue' class='form-control'  name='desc"+ value +"'>  </div><br></div>");
                       $("#produtos").prop('disabled', false);
                    })
                    .fail(function(jqXHR, textStatus, msg){
                        swal(msg, textStatus);
                    });
                }                 
                 
            });            
            
         });
            
        @if(session()->has('status') && session()->get('status') == 200)
        swal("Sucesso!", "{{ session()->get('msg') }}", "success");
        @endif
        @if(session()->has('status') && session()->get('status') == 400)
        swal("Erro!", "{{ session()->get('msg') }}", "error");
        @endif

        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.num').mask('0000000000');
        $('.celular').mask('(00) 0 0000-0000');
        $('.telefone').mask('(00) 0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0.00", {reverse: true});
        $('.quantidade').mask("#.##0.000", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
        translation: {
        'Z': {
        pattern: /[0-9]/, optional: true
        }
        }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
        translation: {
        'r': {
        pattern: /[\/]/,
        fallback: '/'
        },
        placeholder: "__/__/____"
        }
        });
        $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
        </script>
@endsection
