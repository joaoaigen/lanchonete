<?php
use Illuminate\Http\Request;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return redirect('login');
});

Route::get('/reservar-mesa/{id}', 'Admin\MesasController@create')->name('cadastros.reservar-mesa.edit');
Route::get('/imprimir', 'Admin\ImpressoraController@impressao');
Route::get('/impresso', 'Admin\ImpressoraController@impresso');
Route::get('/lista-usuarios', function(){
    return response()->json(DB::table('users')->select('username')->get());
});

Auth::routes();

Route::get('/logout', function() {
    Auth::logout();

    return redirect('login');
});

Route::get('/configuracao-empresa', function() {
    return view('config_empresa');
})->name('admin.config.empresa');

Route::post('/cadastrar/empresa', 'Admin\EmpresaController@store')->name('admin.cadastrar.empresa');


Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::prefix('usuario')->group(function () {
            Route::get('perfil', function(){
                return view('perfil');
            })->name('usuario.perfil');   
            
            Route::post('perfil', 'Admin\UsuarioController@perfil')->name('user.perfil');
    });
    
    Route::group(['middleware' => ['auth', 'admin']], function () {
        Route::get('/home', 'Admin\HomeController@index')->name('admin.home');
        
        Route::prefix('pedidos')->group(function () {            
            Route::get('/cancelar-pedido/{id}', 'Admin\PedidosController@destroy');
            Route::post('/editar-pedido/', 'Admin\PedidosController@update')->name('pedidos.editar');
            Route::get('/editar-pedido/{id}', function($id){                
                return view('editar_pedidos')
                        ->with('dados', \App\Pedidos::where('id', '=', $id)->first())
                        ->with('produtos', App\Produto::all())
                        ->with('id_produtos', DB::table('produto')->select('id')->get())
                        ->with('id', $id);
            });
            Route::get('/fechar-pedido/{id}', 'Admin\PedidosController@create');
            Route::post('/fechar-pedido', 'Admin\PedidosController@edit')->name('fechar.pedido');
        });  
    });
    
    Route::get('gerar-qrcode/{id}', function($id) {
        $qr_image = '';
        
        $renderer = new \BaconQrCode\Renderer\ImageRenderer(
                new \BaconQrCode\Renderer\RendererStyle\RendererStyle(300), new BaconQrCode\Renderer\Image\ImagickImageBackEnd()
        );
        $writer = new \BaconQrCode\Writer($renderer);

        if (isset($writer)) {
            $qr_image = base64_encode($writer->writeString('http://lanchonete.nsystemsolutions.com.br/reservar-mesa/' . $id));
            
        } else {
            $qr_image = null;            
        }       
        
        return back()->with('mesa', $id)->with('qrCode', $qr_image);
    });
        
    Route::prefix('financeiro')->group(function () {
        Route::get('contas-pagar', function(){    
            $contas = DB::table("contaspagar")->get();
           return view('financeiro.contas_pagar')->with('contas', $contas);
        });
        
        Route::post('contas-pagar', 'FinanceiroController@contasPagar')->name('contas.pagar');
    });

    Route::prefix('pedidos')->group(function () {
        Route::get('/calcular-pedido', 'Admin\PedidosController@calcular_pedido');
        Route::get('/cancelar-pedido', 'Admin\PedidosController@calcular_pedido');
    });
    
    Route::prefix('produtos')->group(function () {
        Route::get('/pesquisa', 'Admin\ProdutoController@show');
    });
    
    Route::prefix('cadastros')->group(function () {        
        Route::prefix('usuario')->group(function () {
            Route::get('/', 'Admin\UsuarioController@index')->name('cadastros.usuario.view');
            Route::post('/', 'Admin\UsuarioController@store')->name('cadastros.usuario.store');
        });

        Route::prefix('filial')->group(function () {
            Route::get('/', 'Admin\EmpresaController@index')->name('cadastros.filial.view');
            Route::post('/', 'Admin\EmpresaController@filial')->name('cadastros.filial.store');
        });

        Route::prefix('produto')->group(function () {
            Route::get('/', 'Admin\ProdutoController@index')->name('cadastros.produto.view');
            Route::post('/', 'Admin\ProdutoController@store')->name('cadastros.produto.store');
        });

        Route::prefix('insumo')->group(function () {
            Route::get('/', 'Admin\InsumosController@index')->name('cadastros.insumo.view');
            Route::post('/', 'Admin\InsumosController@store')->name('cadastros.insumo.store');
        });

        Route::prefix('estoque')->group(function () {
            Route::get('/', 'Admin\EstoqueController@index')->name('cadastros.estoque.view');
            Route::post('/', 'Admin\EstoqueController@store')->name('cadastros.estoque.store');
        });

        Route::prefix('mesas')->group(function () {
            Route::get('/', 'Admin\MesasController@index')->name('cadastros.mesas.view');
            Route::post('/', 'Admin\MesasController@store')->name('cadastros.mesas.store');
        });
        
        Route::prefix('pedidos')->group(function () {
            Route::get('/', 'Admin\PedidosController@index')->name('cadastros.pedidos.view');
            Route::post('/', 'Admin\PedidosController@store')->name('cadastros.pedidos.store');
        });
        
        Route::prefix('categorias')->group(function () {
            Route::get('/', 'Admin\CategoriaController@index')->name('cadastros.categoria.view');
            Route::post('/', 'Admin\CategoriaController@store')->name('cadastros.categoria.store');
        });
    });
});
