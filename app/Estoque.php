<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estoque extends Model
{
    protected $fillable = [
        'id', 'insumo', 'quantidade'
    ];   
    
    protected $table = 'estoque';
}
