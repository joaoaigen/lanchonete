<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'nome', 'cnpj', 'telefone', 'celular', 'cep', 'rua', 'bairro', 'cidade', 'uf', 'status', 'nome_exib', 'created_at', 'updated_at', 'deleted_at', 'numero_cep'
    ];   
    
    protected $table = 'empresas';
    
    public $autoincrement = true;
}
