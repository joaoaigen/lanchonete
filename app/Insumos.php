<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insumos extends Model
{
     protected $fillable = [
        'id', 'nome', 'descricao'
    ];   
    
    protected $table = 'insumos';
}
