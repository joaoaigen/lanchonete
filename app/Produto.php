<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = [
        'nome', 'preco', 'preco_custo', 'categoria'
    ];   
    
    protected $table = 'produto';
}
