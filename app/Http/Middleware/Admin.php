<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        
        if (Auth::check() && Auth::user()->empresa == null) {
            return redirect('/configuracao-empresa');
        }
        
        if(Auth::check() && Auth::user()->tipo != 'Administrador' && Auth::user()->tipo != 'Master'){
            dd('colaborador');
        }        
        
        return $next($request);
    }
}
