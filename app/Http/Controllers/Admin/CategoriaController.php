<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categoria;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class CategoriaController extends Controller
{
    
    public function __construct() {
        date_default_timezone_set( 'America/Sao_Paulo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoria = Categoria::all();
        
        return view('admin.categoria')->with('categoria', $categoria);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'nome' => 'required|unique:categoria_produto',
        ]);

        if ($validator->fails()) {            
            return redirect('cadastros/categorias')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        if(Categoria::create([
            'nome' => $request->nome,
            'created_at' => date('Y-m-d  H:i:s'),
            'updated_at' => date('Y-m-d  H:i:s'),
            'criado_por' => Auth::user()->id,
        ])){
            return back()->with('status', 200)->with('msg', 'Categoria cadastrada com sucesso!');
        }else{
            return back()->with('status', 400)->with('msg', 'Não foi possível cadastrar esta categoria!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
