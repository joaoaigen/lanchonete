<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Empresa;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller
{
    
    public function __construct() {
        date_default_timezone_set( 'America/Sao_Paulo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.filial')->with('empresa', Empresa::where('id', '=', Auth::user()->empresa)->get())->with('filial', Empresa::where('matriz', '=', Auth::user()->empresa)->where('filial', '=', 1)->get() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'nome_exib' => 'required|max:255',
            'cnpj' => 'required|unique:empresas',
        ]);

        if ($validator->fails()) {
            return redirect('configuracao-empresa')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $store = Empresa::create([
                    'nome' => isset($request->nome) ? $request->nome : null,
                    'nome_exib' => $request->nome_exib,
                    'cnpj' => $request->cnpj,
                    'telefone' => isset($request->telefone) ? $request->telefone : null,
                    'celular' => $request->celular,
                    'cep' => $request->cep,
                    'rua' => $request->rua,
                    'bairro' => $request->bairro,
                    'cidade' => $request->cidade,
                    'uf' => $request->uf,
                    'numero_cep' => $request->numero_cep,
                    'status' => 'Aguardando Pagamento',
        ]);

        User::where('id', '=', Auth::user()->id)->update([
           'empresa' => $store->id 
        ]);

        return redirect('/home')->with('status', 200)->with('msg', 'Cadastro realizado com sucesso! Efetue o pagamento para utilizar todos os nossos recursos.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function filial(Request $request){
        $validator = Validator::make($request->all(), [
            'nome_exib' => 'required|max:255',
            'cnpj' => 'required|unique:empresas',
            'cep' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('configuracao-empresa')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $store = Empresa::create([
                    'nome' => isset($request->nome) ? $request->nome : null,
                    'nome_exib' => $request->nome_exib,
                    'cnpj' => $request->cnpj,
                    'telefone' => isset($request->telefone) ? $request->telefone : null,
                    'celular' => $request->celular,
                    'cep' => $request->cep,
                    'rua' => $request->rua,
                    'bairro' => $request->bairro,
                    'cidade' => $request->cidade,
                    'uf' => $request->uf,
                    'numero_cep' => $request->numero_cep,
                    'filial' => 1,
                    'matriz' => $request->matriz,
                    'status' => 'Aguardando Pagamento',
        ]);

        User::where('id', '=', Auth::user()->id)->update([
           'empresa' => $store->id 
        ]);

        return redirect('/home')->with('status', 200)->with('msg', 'Cadastro realizado com sucesso! Efetue o pagamento para utilizar todos os nossos recursos.');
    }
}
