<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Estoque;
use Illuminate\Support\Facades\Validator;

class EstoqueController extends Controller
{
    
    public function __construct() {
        date_default_timezone_set( 'America/Sao_Paulo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.estoque')->with('estoque', Estoque::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'insumo' => 'required',
                    'quantidade' => 'required',
        ]);

        if ($validator->fails()) {            
            return redirect('cadastros/estoque')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        if(Estoque::create($request->only(['insumo', 'quantidade']))){
            return back()->with('status', 200)->with('msg', 'Estoque cadastrado com sucesso!');
        }else{
            return back()->with('status', 400)->with('msg', 'Não foi possível cadastrar este estoque!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
