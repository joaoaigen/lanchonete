<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mesas;
use App\Pedidos;
use App\User;
use App\Produto;
use App\Categoria;
use App\Impressoras;
use App\Http\Controllers\Admin\ImpressoraController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use NFePHP\NFe\Make;

class PedidosController extends Controller
{
    
    public function __construct() {
        date_default_timezone_set( 'America/Sao_Paulo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pedidos')
                ->with('mesas', Mesas::all())
                ->with('pedidos', Pedidos::all())
                ->with('usuario', User::where('tipo', 'Cliente')->get())
                ->with('produtos', Produto::all())                
                ->with('id_produtos', DB::table('produto')->select('id')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $dados = Pedidos::where('id', '=', $id)->first();
        
        return view('fechar_pedido')
                ->with('dados', $dados)
                ->with('mesas', Mesas::all())
                ->with('produtos', Produto::all())
                ->with('usuario', User::where('tipo', 'Cliente')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [                    
                    'usuario' => 'required',
                    'produtos' => 'required',
                    'tipo' => 'required',
                    'total' => 'required',
        ]);        
        
        if ($validator->fails()) {            
            return redirect('cadastros/pedidos')
                            ->withErrors($validator)
                            ->withInput();
        }
                
        foreach($request->produtos as $key => $row){ 
            $desc = 'desc'.$row;            
            if(isset($request->$row)){                
                $arr[$key]['produto'] = $row;
                $arr[$key]['quantidade'] = $request->$row;
                $arr[$key]['desc'] = $request->$desc;
            }
        }
        
        if (User::where('username', '=', $request->usuario)->first()) {
            $usr = User::where('username', '=', $request->usuario)->first();
        }else{
            $usr = User::create([
                        'username' => $request->usuario,
                        'tipo' => 'Cliente',
                        'password' => bcrypt('123456'),
                        'status' => 'Ativo',
                        'empresa' => Auth::user()->empresa
            ]);            
        }

        $result = Pedidos::create([
           'empresa' => Auth::user()->empresa, 
           'usuario' => $usr->id, 
           'produtos' => json_encode($arr), 
           'mesa' => isset($request->mesa) ? $request->mesa : null, 
           'tipo' => $request->tipo, 
           'status_viagem' => $request->tipo == 'Viagem' ? 'Gerado' : null, 
           'status_local' => $request->tipo == 'Local' ? 'Gerado' : null, 
           'status' => 'Aberto',
           'total' => $request->total,
           'created_at' => date('Y-m-d H:i:s'), 
           'updated_at' => date('Y-m-d H:i:s'), 
           'criado_por' => Auth::user()->id, 
           'atualizado_por' => Auth::user()->id,            
        ]);
        
        
        if($request->tipo == 'Local'){
            Mesas::where('id', '=', $request->mesa)->update([
                'status' => 'Ocupado',
                'updated_at' => date('Y-m-d H:i:s'),
                'atualizado_por' => Auth::user()->id,
            ]);
            
            
            
            foreach ($request->produtos as $i => $row) {
                $produto = Produto::where('id', '=', $row)->first();
                $desc = 'desc'.$row; 
                //informações para impressão
                $data[$produto->categoria]['impressora'] = Impressoras::where('categoria', '=', $produto->categoria)->where('status', '=', 'Ativo')->first()->nome;
                $data[$produto->categoria]['mesa'] = $result->mesa;
                $data[$produto->categoria]['tipo'] = $request->tipo;
                
                //informações do produto
                $data[$produto->categoria]['categoria'] = $produto->categoria;
                $data[$produto->categoria]['id'] = $result->id;
                $data[$produto->categoria]['produtos'][$i]['nome'] = $produto->nome;
                $data[$produto->categoria]['produtos'][$i]['qntd'] = $request->$row;
                $data[$produto->categoria]['produtos'][$i]['desc'] = $request->$desc;
            }
            
            DB::table('impressao')->insert([
                'produtos' => json_encode($data),
                'created_at' => $result->created_at,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        
        return back()->with('status', 200)->with('msg', 'Pedido criado com sucesso!');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(isset($request->confirmar) && $request->confirmar == true){
            $total = 0;
            $quantidade = 0;
            $quantidade_concluidos = 0;
            $pedido = Pedidos::where('id', '=', $request->id)->first();
            
            foreach(json_decode($pedido->produtos) as $key => $row){
                $produto = $row->produto;
                if(isset($request->$produto)){
                    $arr[$key]['id'] = $row->produto;
                    $arr[$key]['produto'] = Produto::where('id', '=', $row->produto)->first()->nome;
                    $arr[$key]['quantidade'] = $request->$produto;
                    $arr[$key]['valor'] = Produto::where('id', '=', $row->produto)->first()->preco;
                    $quantidade_concluidos = $request->$produto + $quantidade_concluidos;
                }
                $quantidade = $row->quantidade + $quantidade;
                $multi = DB::table('produto')->where('id', '=', $row->produto)->first()->preco * $request->$produto;
                $total = $total + $multi;
            }

            /*DB::table('impressao')->insert([
                'produtos' => json_encode($arr),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'pedido' => $pedido->id,
            ]);*/
            
            return response()->json([
                'mesa' => Mesas::where('id', '=', $request->mesa)->first()->descricao,
                'usuario' => User::where('id', '=' , $request->usuario)->first()->username,
                'produtos' => $arr,
                'total' => $total,
                'pedido' => $pedido->id
            ]);
        }  else {            
            $pedido = Pedidos::where('id', '=', $request->pedido)->first();

            foreach (json_decode($pedido->produtos) as $key => $row) {
                $produto = $row->produto;

                if (isset($request->$produto) && $request->$produto > 0) {                    
                    if ($request->$produto >= $row->quantidade) { 
                        
                        $concluido[$key]['produto'] = $row->produto;
                        $concluido[$key]['quantidade'] = $request->$produto;
                        $concluido[$key]['desc'] = $row->desc;
                        
                    } else if ($request->$produto < $row->quantidade && $request->$produto > 0) {
                        $concluido[$key]['produto'] = $row->produto;
                        $concluido[$key]['quantidade'] = $request->$produto;
                        $concluido[$key]['desc'] = $row->desc;                        
                        
                        $nao_concluido[$key]['produto'] = $row->produto;
                        $nao_concluido[$key]['quantidade'] = $row->quantidade - $request->$produto;
                        $nao_concluido[$key]['desc'] = $row->desc;                        
                    }
                }else{ 
                    
                    $nao_concluido[$key]['produto'] = $row->produto;
                    $nao_concluido[$key]['quantidade'] = $row->quantidade;
                    $nao_concluido[$key]['desc'] = $row->desc;
                }  
            }
                
            if (isset($nao_concluido)) {
                Pedidos::where('id', '=', $pedido->id)->update([
                    'produtos' => json_encode($nao_concluido),
                    'produtos_concluidos' => isset($concluido) ? json_encode($concluido) : null,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'atualizado_por' => Auth::user()->id
                ]);
                
                DB::table('impressao')->insert([
                    'produtos' => json_encode($concluido),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'pedido' => $pedido->id,
                    'fiscal' => 1,
                ]);

                return back()->with('status', 200)->with('msg', 'Enviado a fila de impressão!');                
            } else {
                
                Pedidos::where('id', '=', $pedido->id)->update([
                    'produtos' => json_encode($concluido),
                    'produtos_concluidos' => json_encode($concluido),
                    'status' => 'Concluido',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'atualizado_por' => Auth::user()->id
                ]);

                Mesas::where('id', '=', $pedido->mesa)->update([
                    'status' => 'Disponivel',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'atualizado_por' => Auth::user()->id
                ]);
                
                DB::table('impressao')->insert([
                    'produtos' => json_encode($concluido),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'pedido' => $pedido->id,
                    'fiscal' => 1,
                ]);
                
                session()->put('status', 200);
                session()->put('msg', 'Pedido finalizado com sucesso!');
                
                return redirect('/cadastros/pedidos')->with('status', 200)->with('msg', 'Pedido finalizado com sucesso!');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $pedido = Pedidos::where('id', '=', $request->id)->first();
        $arr1 = json_decode($pedido->produtos);
        
        foreach ($request->produtos as $key => $row) {
            $desc = 'desc' . $row;
            if (isset($request->$row)) {
                $arr[$key]['produto'] = $row;
                $arr[$key]['quantidade'] = $request->$row;
                $arr[$key]['desc'] = $request->$desc;
            }
        }
                
        $novo_arr = array_merge($arr1, $arr);
              
        Pedidos::where('id', '=', $request->id)->update([
            'produtos' => json_encode($novo_arr),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        
        
        foreach ($request->produtos as $i => $row) {
            $produto = Produto::where('id', '=', $row)->first();
            $desc = 'desc' . $row;
            //informações para impressão
            $data[$produto->categoria]['impressora'] = Impressoras::where('categoria', '=', $produto->categoria)->where('status', '=', 'Ativo')->first()->nome;
            $data[$produto->categoria]['mesa'] = $request->mesa;

            //informações do produto
            $data[$produto->categoria]['categoria'] = $produto->categoria;
            $data[$produto->categoria]['id'] = $produto->id;
            $data[$produto->categoria]['produtos'][$i]['nome'] = $produto->nome;
            $data[$produto->categoria]['produtos'][$i]['qntd'] = $request->$row;
            $data[$produto->categoria]['produtos'][$i]['desc'] = $request->$desc;
        }
        
        DB::table('impressao')->insert([
            'produtos' => json_encode($data),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'pedido' => $pedido->id,
        ]);
        
        return back()->with('status', 200)->with('msg', 'Pedido atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mesa = Pedidos::where('id', '=', $id)->first()->mesa;
        Mesas::where('id', '=', $mesa)->update([
           'status' => 'Disponivel' 
        ]);
        Pedidos::where('id', '=', $id)->update([
            'status' => 'Cancelado',
            'updated_at' => date('Y-m-d H:i:s'),
            'atualizado_por' => Auth::user()->id,
        ]);
        
        return back()->with('status', 200)->with('msg', 'Pedido cancelado com sucesso!');
    }
    
   public function calcular_pedido(Request $request){
       $total = 0;
       
       
       foreach($request->produtos as $key => $row){             
          $multi =  DB::table('produto')->where('id', '=', $row['produto'])->first()->preco * $row['qntd'];
          $total = $total + $multi;
       }
       
       echo $total;
   }
   
   public function fecharPedido(Request $request){
       
   }
}
