<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mesas;
use Illuminate\Support\Facades\Validator;

class MesasController extends Controller
{
    
    public function __construct() {
        date_default_timezone_set( 'America/Sao_Paulo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesas = Mesas::all();
        
        
        return view('admin.mesas')->with('mesas', $mesas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $mesa = Mesas::where('id', '=', $id)->first();

        if ($mesa->status == 'Ocupado') {
            return redirect('/')->with('status', 400)->with('msg', 'Está mesa está ocupada! Selecione outra.');
        }

        return view('pedido_usuario')->with('mesa', $mesa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'numero' => 'required|unique:mesas',
        ]);

        if ($validator->fails()) {            
            return redirect('cadastros/mesas')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        if(Mesas::create([
            'numero' => $request->numero,
            'descricao' => isset($request->descricao) ? $request->descricao : null
        ])){
            return back()->with('status', 200)->with('msg', 'Mesa cadastrada com sucesso!');
        }else{
            return back()->with('status', 400)->with('msg', 'Não foi possível cadastrar este insumo!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
