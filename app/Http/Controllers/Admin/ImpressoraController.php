<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ImpressoraController extends Controller
{
    public function impressao(){
        $impressao = DB::table('impressao')->where('status', '=', 'Nao impresso')->first();
        
        
        
        return response()->json([
            'impressao' => $impressao->id,
            'produtos' => json_decode($impressao->produtos),
            'created_at' => $impressao->created_at,
            'pedido' => $impressao->pedido,
            'fiscal' => $impressao->fiscal,
            'impressora' => $impressao->fiscal == 1 ? DB::table('impressoras')->where('fiscal', '=', 'Sim')->first()->nome : null,
        ]);
    }
    
    public function impresso(Request $request){
        $result = DB::table('impressao')->where('id', '=', $request->id)->update([
            'status' => 'Impresso',
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        
        return $result;
    }
}
