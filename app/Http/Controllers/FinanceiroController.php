<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContasPagar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FinanceiroController extends Controller
{
    public function contasPagar(Request $request){
        $validator = Validator::make($request->all(), [
                    'nome' => 'required',
                    'descricao' => 'required',
                    'valor' => 'required',
        ]);

        if ($validator->fails()) {            
            return redirect('financeiro/contas-pagar')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $data = date('Y-m-d');
        
        if(date('Y-m-d', strtotime($request->data_vencimento)) < $data){
           $status = 'Vencido'; 
        }else{
           $status = 'A vencer';
        }
        
        if(ContasPagar::create([
            'nome' => $request->nome,
            'descricao' => $request->descricao,
            'status' => $status,
            'data_vencimento' => date('Y-m-d', strtotime($request->data_vencimento)),
            'valor' => $request->valor,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
            'criado_por' => Auth::user()->id,
            'atualizado_por' => Auth::user()->id,
        ])){
            return back()->with('status', 200)->with('msg', 'Conta cadastrada com sucesso!');
        }else{
            return back()->with('status', 400)->with('msg', 'Não foi possível cadastrar esta conta!');
        }
    }
}
