<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configs extends Model
{
    protected $fillable = [
        'empresa', 'nome_exib_clientes', 'cep_obrigatorio', 'celular_obrigatorio', 'telefone_obrigatorio', 'logo_empresa'
    ];   
    
    protected $table = 'configs';
}
