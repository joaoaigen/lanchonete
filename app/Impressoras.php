<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Impressoras extends Model
{
    protected $fillable = [
        'id', 'nome', 'categoria', 'status'
    ];   
    
    protected $table = 'impressoras';
}
