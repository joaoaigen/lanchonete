<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContasPagar extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'status', 'data_vencimento', 'data_pagamento', 'valor', 'created_at', 'updated_at', 'criado_por', 'atualizado_por' 
    ];
    
    protected $table = 'contaspagar';
    
    public $autoincrement = true;

}
