<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $fillable = [
        'empresa', 'usuario', 'produtos', 'mesa', 'tipo', 'status_viagem', 'status_local', 'status', 'total', 'criado_por'
    ];   
    
    protected $table = 'pedidos';
}
